package domain;
import java.util.List;

public class Country {

    private String name;
    private Long id;


    private List<CountryWifi>wifi;

    public List<CountryWifi> getWifi() {
        return wifi;
    }

    public void setWifi(List<CountryWifi> wifi) {
        this.wifi = wifi;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
