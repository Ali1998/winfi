package domain;

public class Constant {

    public static final Integer INTERNAL_EXCEPTION=2000;
    public static final Integer ERROR=1001;
    public static final Integer SUCCESS=1000;
    public static final Integer SESSION_END=1002;
    public static final Integer EmptyData=1008;
    public static final Integer NotInformation=1014;
    public static final Integer CurrentData=1007;
    public static final Integer WrongTransactionId=1009;
    public static final Integer MaxLength=1012;
    public static final Integer InvalidValue=1020;
    public static final Integer WrongAmount=1011;
    public static final Integer InvalidRequest=1004;
    public static final Integer PaymentStatus=1003;


}
