package domain;

public class Report {

    private  int todayCount;
    private String  todayDate;
    private int monthlyCount;
    private String  monthlyDate;
    private int yearlyCount;
    private String  yearlyDate;
    private int totalCount;
    private String  totalDate;
    private double todayEarning;
    private double totalEarning;


    public double getTodayEarning() {
        return todayEarning;
    }

    public void setTodayEarning(double todayEarning) {
        this.todayEarning = todayEarning;
    }

    public double getTotalEarning() {
        return totalEarning;
    }

    public void setTotalEarning(double totalEarning) {
        this.totalEarning = totalEarning;
    }

    public int getTodayCount() {
        return todayCount;
    }

    public void setTodayCount(int todayCount) {
        this.todayCount = todayCount;
    }

    public String getTodayDate() {
        return todayDate;
    }

    public void setTodayDate(String todayDate) {
        this.todayDate = todayDate;
    }

    public int getMonthlyCount() {
        return monthlyCount;
    }

    public void setMonthlyCount(int monthlyCount) {
        this.monthlyCount = monthlyCount;
    }

    public String getMonthlyDate() {
        return monthlyDate;
    }

    public void setMonthlyDate(String monthlyDate) {
        this.monthlyDate = monthlyDate;
    }

    public int getYearlyCount() {
        return yearlyCount;
    }

    public void setYearlyCount(int yearlyCount) {
        this.yearlyCount = yearlyCount;
    }

    public String getYearlyDate() {
        return yearlyDate;
    }

    public void setYearlyDate(String yearlyDate) {
        this.yearlyDate = yearlyDate;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public String getTotalDate() {
        return totalDate;
    }

    public void setTotalDate(String totalDate) {
        this.totalDate = totalDate;
    }
}
