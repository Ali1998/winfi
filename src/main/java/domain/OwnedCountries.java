package domain;
import java.util.List;

public class OwnedCountries {


    private long id;
    private String country_name;
    private String finishDate;
    private List<OwnedWifi>wifi;
    private String finishTime;

    public String getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(String finishTime) {
        this.finishTime = finishTime;
    }

    public List<OwnedWifi> getWifi() {
        return wifi;
    }

    public void setWifi(List<OwnedWifi> wifi) {
        this.wifi = wifi;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCountry_name() {
        return country_name;
    }

    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }

    public String getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(String finishDate) {
        this.finishDate = finishDate;
    }
}
