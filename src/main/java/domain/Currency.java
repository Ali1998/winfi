package domain;

public class Currency {

    private String name;
    private double convertPrice;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getConvertPrice() {
        return convertPrice;
    }

    public void setConvertPrice(double convertPrice) {
        this.convertPrice = convertPrice;
    }
}
