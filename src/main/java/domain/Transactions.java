package domain;

import com.sun.istack.internal.NotNull;
import javafx.beans.DefaultProperty;
import org.springframework.beans.factory.annotation.Value;

public class Transactions {

   private Double amount;
   private Integer status;
   private String createdDate;
   private String accountName;

   private String description;

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }


    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        if(description==null){
            description="";
        }
        this.description = description;
    }
}
