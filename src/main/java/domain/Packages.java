package domain;

public class Packages {


    private Integer duration;
    private Integer packId;
    private String  price;
    private String  convertPrice;
    private String  currencyName;

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public String getConvertPrice() {
        return convertPrice;
    }

    public void setConvertPrice(String convertPrice) {
        this.convertPrice = convertPrice;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Integer getPackId() {
        return packId;
    }

    public void setPackId(Integer packId) {
        this.packId = packId;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
