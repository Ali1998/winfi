package util;

import domain.Cryptography;
import domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import service.CountryService;
import service.UserService;

import java.net.URLEncoder;
import java.util.Random;
import java.util.UUID;
import java.util.regex.Pattern;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.util.Base64;


public class Methods {




    public static boolean isNumber (String msisdn)
    {
        String regex="\\d+";
        Pattern p=Pattern.compile(regex);
        return p.matcher(msisdn).matches();
    }








    public static String generateCode()
    {
        String text="";
        try{


            Random generator = new java.util.Random();
            generator.setSeed(System.currentTimeMillis());
            Integer i = generator.nextInt(900000) + 100000;
            text = URLEncoder.encode(String.valueOf(i), "UTF-8");

        }catch (Exception e){
            e.printStackTrace();
        }

        return text;
    }




    public static String encrypt(String psw, Cryptography cryptography) {
        try {
            IvParameterSpec iv = new IvParameterSpec(cryptography.getInitVector().getBytes("UTF-8"));
            SecretKeySpec skeySpec = new SecretKeySpec(cryptography.getKey().getBytes("UTF-8"), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);

            byte[] encrypted = cipher.doFinal(psw.getBytes());
            return Base64.getEncoder().encodeToString(encrypted);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }


    public static String decrypt(String encrypted, Cryptography cryptography) {
        try {
            IvParameterSpec iv = new IvParameterSpec(cryptography.getInitVector().getBytes("UTF-8"));
            SecretKeySpec skeySpec = new SecretKeySpec(cryptography.getKey().getBytes("UTF-8"), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
            byte[] original = cipher.doFinal(Base64.getDecoder().decode(encrypted.getBytes()));

            return new String(original);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }


    public static String getSecurityKey(){

        String result= UUID.randomUUID()
                .toString()
                .replace("-","")
                .substring(0,16);

        return result;
    }





















}
