package util;


import domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import service.CountryService;
import service.UserService;

@Service
public class MethodService {

    @Autowired
    private  UserService userService;

    @Autowired
    private  CountryService countryService;



    public boolean checkType(Integer id,String transactionId){

        User user=userService.getPhone(transactionId);
        String number="+"+user.getMsisdn().substring(0,user.getMsisdn().length()-9);
        return countryService.localCheck(number,id);
    }





}
