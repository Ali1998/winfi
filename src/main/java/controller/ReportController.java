package controller;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import domain.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import service.HttpService;
import service.ReportService;
import service.UserService;
import java.util.HashMap;
import java.util.Map;


@RestController
public class ReportController {

    @Autowired
    private ReportService reportService;
    @Autowired
    private HttpService service;
    @Autowired
    private UserService userService;



    private static final Logger logger =Logger.getLogger(ReportController.class);

    @RequestMapping(value = "/report/getReports", method = RequestMethod.POST, produces = "application/json")
    public Map<String, Object> getReportList() {


        Map<String, Object> data = new HashMap<>();
        Map<String, String> map;
        ObjectMapper mapper = new ObjectMapper();
        try {

            map = mapper.readValue(service.getHeadersInfo().get(Config.HEADER_KEY), new TypeReference<Map<String, String>>() {
            });
            User user = new User();
            String transactionId=map.get(Config.TRANSACTION_KEY);
            user.setTransactionId(transactionId);
            if (userService.checkUser(user)) {
                Report todayReport = reportService.getTodayReport(transactionId);
                Report monthlyReport = reportService.getMonthlyReport(transactionId);
                Report yearlyReport = reportService.getYearlyReport(transactionId);
                Report totalReport = reportService.getTotalReport(transactionId);

                data.put("statusCode", Constant.SUCCESS);
                data.put("todayConnectionCount", todayReport.getTodayCount());
                data.put("todayConnectionDate", todayReport.getTodayDate());
                data.put("monthlyConnectionCount", monthlyReport.getMonthlyCount());
                data.put("monthlyConnectionDate", monthlyReport.getMonthlyDate());
                data.put("yearlyConnectionCount", yearlyReport.getYearlyCount());
                data.put("yearlyConnectionDate", "last " + yearlyReport.getYearlyDate());
                data.put("totalConnectionCount", totalReport.getTotalCount());
                if (totalReport.getTotalDate() != null) {
                    data.put("totalConnectionDate", totalReport.getTotalDate());
                } else {
                    data.put("totalConnectionDate", "No date");

                }

            } else {
                data.put("statusCode", Constant.WrongTransactionId);
                data.put("statusMessage", "Wrong TransactionId");
            }


        } catch (Exception ex) {
            data.put("statusCode", Constant.INTERNAL_EXCEPTION);
            data.put("statusMessage", "System Error");
            logger.info("error:"+ex);
        }
        return data;

    }


    @RequestMapping(value = "/report/getEarning", method = RequestMethod.POST, produces = "application/json")
    public Map<String, Object> getEarningList() {


        Map<String, Object> data = new HashMap<>();
        Map<String, String> map;
        ObjectMapper mapper = new ObjectMapper();

        try {

            map = mapper.readValue(service.getHeadersInfo().get(Config.HEADER_KEY), new TypeReference<Map<String, String>>() {
            });
            User user = new User();
            String transactionId=map.get(Config.TRANSACTION_KEY);
            user.setTransactionId(transactionId);


            if (userService.checkUser(user)) {
                Report todayEarning = reportService.getTodayEarning(transactionId);
                Report totalEarning = reportService.getTotalEarning(transactionId);
                Operation pendingInfo=reportService.getPendingInfo(transactionId);
                Operation success=reportService.getSuccessInfo(transactionId);
                Operation operation=reportService.getConnectionMaxDate(transactionId);
                double paid=success.getAmount();
                double pending=pendingInfo.getAmount();

                data.put("statusCode", Constant.SUCCESS);
                data.put("todayEarningCount", String.valueOf(todayEarning.getTodayEarning()));
                data.put("todayEarningDate", todayEarning.getTodayDate());
                data.put("balanceEarningCount", String.format("%.1f",totalEarning.getTotalEarning()-paid-pending));
                data.put("withdrawnEarningCount",String.valueOf(success.getAmount()));
                data.put("pendingEarningsCount", String.valueOf(pendingInfo.getAmount()));
                data.put("totalEarningCount", String.valueOf(totalEarning.getTotalEarning()));

                if(operation.getMaxDate()!=null) {
                    data.put("balanceEarningDate", operation.getMaxDate());
                }else{
                    data.put("balanceEarningDate", "No date");
                }
                if(success.getResultDate()!=null){
                    data.put("withdrawnEarningDate",success.getResultDate());
                }else{
                    data.put("withdrawnEarningDate","No date");
                }

                if (pendingInfo.getPendingDate() != null) {
                    data.put("pendingEarningsDate", pendingInfo.getPendingDate());
                } else {
                    data.put("pendingEarningsDate", "No date");
                }


                if (totalEarning.getTotalDate() != null) {
                    data.put("totalEarningDate", totalEarning.getTotalDate());
                } else {
                    data.put("totalEarningDate", "No date");
                }

            } else {
                data.put("statusCode", Constant.WrongTransactionId);
                data.put("statusMessage", "Wrong TransactionId");
            }


        } catch (Exception ex) {
            data.put("statusCode", Constant.INTERNAL_EXCEPTION);
            data.put("statusMessage", "System Error");
            logger.info("error:"+ex);
        }
        return data;

    }


}
