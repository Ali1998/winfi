package controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import domain.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import service.ConfigService;
import service.HttpClientService;
import service.HttpService;
import service.UserService;
import util.GsonInitializer;
import util.Methods;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletResponse;
import java.lang.Exception;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;



@RestController
public class UserController {



    @Autowired
    private UserService userService;

    @Autowired
    private HttpClientService clientService;

    @Autowired
    private HttpService service;

    @Autowired
    private ConfigService configService;



    private static Logger logger = Logger.getLogger(UserController.class);

    @RequestMapping(value = "/user/add", method = RequestMethod.POST, produces = "application/json")
    public Map<String, Object> addUser(@RequestBody Map<String,Object>objectMap ) {
        Map<String, Object> map = new HashMap<>();
        User user=new User();
        try {

            int count = userService.getCountDown();
            user.setRoleId(Integer.parseInt(objectMap.get("roleId").toString()));
            user.setMsisdn(objectMap.get("msisdn").toString());



            //mock data
            if(user.getMsisdn().equalsIgnoreCase("994509998877")){
                user.setSmscode("654123");
                if(userService.checkRole(user.getRoleId())){
                    if (userService.checkUserData(user)) {

                        boolean result = userService.saveUser(user);

                        if (result) {
                            map.put("statusCode", Constant.SUCCESS);
                            map.put("statusMessage", "success");
                            map.put("countdown", count);
                        }

                    } else {
                        boolean result = userService.addUser(user);
                        if (result) {
                            map.put("statusCode", Constant.SUCCESS);
                            map.put("statusMessage", "success");
                            map.put("countdown", count);
                        }
                    }
                }else{
                    map.put("statusCode", Constant.NotInformation);
                    map.put("statusMessage", "Such a role was not found");
                }

                return map;

            }



            //mock data android

            else if(!user.getMsisdn().equalsIgnoreCase("994509998877") &&
                    !user.getMsisdn().contains("994")){
                user.setSmscode("123456");
                if(userService.checkRole(user.getRoleId())){
                    if (userService.checkUserData(user)) {

                        boolean result = userService.saveUser(user);

                        if (result) {
                            map.put("statusCode", Constant.SUCCESS);
                            map.put("statusMessage", "success");
                            map.put("countdown", count);
                        }

                    } else {
                        boolean result = userService.addUser(user);
                        if (result) {
                            map.put("statusCode", Constant.SUCCESS);
                            map.put("statusMessage", "success");
                            map.put("countdown", count);
                        }
                    }
                }else{
                    map.put("statusCode", Constant.NotInformation);
                    map.put("statusMessage", "Such a role was not found");
                }

                return map;

            }



            ///

            if (user.getMsisdn()== null) {
                map.put("statusCode", Constant.EmptyData);
                map.put("statusMessage", "Incorrect field");

            }else if(user.getMsisdn().length()<4 || user.getMsisdn().length()>23){

                map.put("statusCode", Constant.MaxLength);
                map.put("statusMessage", "msisdn length should minimum is four and max is twenty");

            } else {

                String msisdn = user.getMsisdn().replace(" ", "").replace("+", "");
                user.setMsisdn(msisdn);


                if(Methods.isNumber(msisdn))
                {
                    if(userService.checkRole(user.getRoleId()))
                    {
                        String text=Methods.generateCode();
                        user.setSmscode(text);
                        text="Code "+text;

                        String url = "https://portmanat.az/test/sms/s2a5gf6a5g96a5g6a?number="+msisdn+"&text="+URLEncoder.encode(text, "UTF-8");

                        clientService.sendGet(url);



                        if (userService.checkUserData(user)) {

                            boolean result = userService.saveUser(user);

                            if (result) {
                                map.put("statusCode", Constant.SUCCESS);
                                map.put("statusMessage", "success");
                                map.put("countdown", count);
                            }

                        } else {
                            boolean result = userService.addUser(user);
                            if (result) {
                                map.put("statusCode", Constant.SUCCESS);
                                map.put("statusMessage", "success");
                                map.put("countdown", count);
                            }
                        }
                    }else{
                        map.put("statusCode", Constant.NotInformation);
                        map.put("statusMessage", "Such a role was not found");
                    }


                }else{
                    map.put("statusCode", Constant.InvalidValue);
                    map.put("statusMessage", "msisdn should is number!");
                }

            }

        } catch (Exception ex) {
            logger.info("error:"+ex);
            map.put("statusCode", Constant.INTERNAL_EXCEPTION);
            map.put("statusMessage", "System Error");
        }

        logger.info("Request of UserAdd: " + GsonInitializer.getInstance().toJson(objectMap));
        logger.info("Response of UserAdd : " + GsonInitializer.getInstance().toJson(map));

        return map;
    }


///apply user




    @RequestMapping(value = "/user/apply", method = RequestMethod.POST, produces = "application/json")
    public Map<String, Object> applyUser(@RequestBody Map<String,Object>objectMap,HttpServletResponse response) {
        Map<String, Object> map = new HashMap<>();
        User user=new User();
        try {

            user.setRoleId(Integer.parseInt(objectMap.get("roleId").toString()));
            user.setMsisdn(objectMap.get("msisdn").toString());
            user.setSmscode(objectMap.get("smscode").toString());

            if(user.getMsisdn().equalsIgnoreCase("994509998877") &&
                    user.getSmscode().equalsIgnoreCase("654123")){
                String trId = String.valueOf(UUID.randomUUID());
                map = userService.getUserByTrId(trId, user.getMsisdn(),user.getRoleId());
                map.put("statusCode", Constant.SUCCESS);
                return map;
            }
            else if(!user.getMsisdn().contains("994") &&
                    user.getSmscode().equalsIgnoreCase("123456")){
                String trId = String.valueOf(UUID.randomUUID());
                map = userService.getUserByTrId(trId, user.getMsisdn(),user.getRoleId());
                map.put("statusCode", Constant.SUCCESS);
                return map;

            }






            if (user.getMsisdn()== null || user.getSmscode()==null) {
                map.put("statusCode", Constant.EmptyData);
                map.put("statusMessage", "Incorrect field");

            }else if(user.getMsisdn().length()<5 || user.getMsisdn().length()>23){

                map.put("statusCode", Constant.MaxLength);
                map.put("statusMessage", "msisdn length should minimum five and max twenty ");

            }
            else
            {
                String msisdn = user.getMsisdn().replace(" ", "").replace("+", "");

                user.setMsisdn(msisdn);

                if(Methods.isNumber(msisdn))
                {
                    if(userService.checkRole(user.getRoleId())){

                        if (userService.getUser(user)) {
                            if (userService.isSession(user)) {
                                String trId = String.valueOf(UUID.randomUUID());
                                map = userService.getUserByTrId(trId, user.getMsisdn(),user.getRoleId());
                                map.put("statusCode", Constant.SUCCESS);



                            } else {
                                map.put("statusCode", Constant.SESSION_END);
                                map.put("statusMessage", "Session end");
                            }
                        } else {
                            map.put("statusCode", Constant.ERROR);
                            map.put("statusMessage", "Incorrect sms code");
                        }

                    }else
                    {
                        map.put("statusCode", Constant.NotInformation);
                        map.put("statusMessage", "Such a role was not found");
                    }

                }else{
                    map.put("statusCode", Constant.InvalidValue);
                    map.put("statusMessage", "msisdn should is number!");
                }

            }

        } catch (Exception ex) {
            logger.info("error:"+ex);
            map.put("statusCode", Constant.INTERNAL_EXCEPTION);
            map.put("statusMessage", "System Error");
        }

        logger.info("Request of UserApply : " + GsonInitializer.getInstance().toJson(objectMap));
        logger.info("Response of UserApply : " + GsonInitializer.getInstance().toJson(map));

        return map;
    }


    //set username;

    @RequestMapping(value = "/user/register", method = RequestMethod.POST, produces = "application/json")
    public Map<String, Object> RegisterUser(@RequestBody User user) {
        Map<String, Object> map = new HashMap<>();
        Map<String, String> data;
        ObjectMapper mapper = new ObjectMapper();


        try {

            data = mapper.readValue(service.getHeadersInfo().get(Config.HEADER_KEY), new TypeReference<Map<String, String>>() {
            });
            String transactionId = data.get(Config.TRANSACTION_KEY);
            String username = user.getUsername();
            user.setTransactionId(transactionId);
            if (userService.checkUser(user)) {
                if (username == null || username.trim().length() == 0) {
                    map.put("statusCode", Constant.EmptyData);
                    map.put("statusMessage", "Incorrect field");
                } else {
                    boolean result = userService.applyUser(user);
                    if (result) {
                        map.put("statusCode", Constant.SUCCESS);
                        map.put("statusMessage", "Success");
                    } else {
                        map.put("statusCode", Constant.NotInformation);
                        map.put("statusMessage", "Not information");
                    }
                }

            } else {
                map.put("statusCode", Constant.WrongTransactionId);
                map.put("statusMessage", "Wrong TransactionId");
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            map.put("statusCode", Constant.INTERNAL_EXCEPTION);
            map.put("statusMessage", "System Error");
        }

        logger.info("Response of UserRegister : " + GsonInitializer.getInstance().toJson(map));

        return map;
    }



    @RequestMapping(value = "/subscribe",method = RequestMethod.POST,produces="application/json")
    public Map<String,Object>addSubscribe(@RequestBody Map<String,Object>param){
        Map<String,Object>data=new HashMap<>();
        try{
            String mail=param.get("mail").toString();
            if(userService.addSubscribe(mail)){
                data.put("statusCode",Constant.SUCCESS);
                data.put("statusMessage","Success");
            }

        }catch (Exception ex){
            data.put("statusCode",Constant.INTERNAL_EXCEPTION);
            data.put("statusMessage","System error");
            ex.printStackTrace();
        }

        logger.info("Request of Subscribe : " + GsonInitializer.getInstance().toJson(param));
        logger.info("Response of Subscribe : " + GsonInitializer.getInstance().toJson(data));


        return data;
    }






}
