package controller;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import domain.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import service.CardService;
import service.HttpService;
import service.UserService;
import util.GsonInitializer;


import javax.xml.ws.Response;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class CardController {

    @Autowired
    private CardService cardService;
    @Autowired
    private HttpService service;
    @Autowired
    private UserService userService;



    private static Logger logger = Logger.getLogger(CardController.class);

    @RequestMapping(value = "/card/getList", method = RequestMethod.POST, produces = "application/json")
    public Map<String, Object> getCardList() {

        Map<String, Object> data = new HashMap<>();
        Map<String, String> map;
        ObjectMapper mapper = new ObjectMapper();
        try {


            map = mapper.readValue(service.getHeadersInfo().get(Config.HEADER_KEY), new TypeReference<Map<String, String>>() {
            });
            User user = new User();
            user.setTransactionId(map.get(Config.TRANSACTION_KEY));


            if (userService.checkUser(user)) {
                List<ResponseCardAccounts> cardAccounts = cardService.cardAccounts(map.get(Config.TRANSACTION_KEY));
                if (cardAccounts != null && !cardAccounts.isEmpty()) {
                    data.put("statusCode", Constant.SUCCESS);
                    data.put("accounts", cardAccounts);
                } else {
                    data.put("statusCode", Constant.NotInformation);
                    data.put("statusMessage", "Not information");
                }
            } else {
                data.put("statusCode", Constant.WrongTransactionId);
                data.put("statusMessage", "Wrong TransactionId");
            }


        } catch (Exception ex) {
            data.put("statusCode", Constant.INTERNAL_EXCEPTION);
            data.put("statusMessage", "System Error");
            logger.info("error:"+ex);
        }
        return data;

    }


    //ADD BANK

    @RequestMapping(value = "/card/add", method = RequestMethod.POST, consumes = "application/json")
    public Map<String, Object> addCardAccount(@RequestBody CardAccount cardAccount) {
        Map<String, Object> data = new HashMap<>();
        Map<String, String> map;
        ObjectMapper mapper = new ObjectMapper();


        try {

            map = mapper.readValue(service.getHeadersInfo().get(Config.HEADER_KEY), new TypeReference<Map<String, String>>() {
            });
            User user = new User();
            user.setTransactionId(map.get(Config.TRANSACTION_KEY));
            cardAccount.setTransactionId(map.get(Config.TRANSACTION_KEY));
            if (userService.checkUser(user)) {
                if (cardAccount.getCardnumber() == null || cardAccount.getCardnumber().trim().length()<16 ||
                        cardAccount.getName() == null || cardAccount.getName().trim().length()==0) {
                    data.put("statuscode", Constant.EmptyData);
                    data.put("statusMessage", "Incorrect field");

                } else if (cardService.addCartAccounts(cardAccount)) {
                    data.put("statusCode", Constant.SUCCESS);
                    data.put("statusMessage", "Success");
                }
            } else {
                data.put("statusCode", Constant.WrongTransactionId);
                data.put("statusMessage", "Wrong TransactionId");
            }


        } catch (Exception ex) {
            data.put("statusCode", Constant.INTERNAL_EXCEPTION);
            data.put("statusMessage", "System Error");
            logger.info("error:"+ex);
        }

        logger.info("Response of CardAdd : " + GsonInitializer.getInstance().toJson(data));

        return data;
    }

    //Edit Card

    @RequestMapping(value = "/card/edit", method = RequestMethod.POST, consumes = "application/json")
    public Map<String, Object> editCard(@RequestBody Map<String,Object>objectMap ) {
        Map<String, Object> data = new HashMap<>();
        Map<String, String> map;
        ObjectMapper mapper = new ObjectMapper();
        CardAccount cardAccount=new CardAccount();

        try {

            map = mapper.readValue(service.getHeadersInfo().get(Config.HEADER_KEY), new TypeReference<Map<String, String>>() {
            });
            cardAccount.setTransactionId(map.get(Config.TRANSACTION_KEY));
            User user = new User();
            user.setTransactionId(map.get(Config.TRANSACTION_KEY));
            cardAccount.setId(Integer.parseInt(objectMap.get("id").toString()));
            cardAccount.setCardnumber(objectMap.get("cardnumber").toString());
            cardAccount.setName(objectMap.get("name").toString());


            if (userService.checkUser(user)) {

                if (cardAccount.getCardnumber() == null || cardAccount.getCardnumber().trim().length()<16 ||
                        cardAccount.getName() == null || cardAccount.getName().trim().length()==0) {
                    data.put("statusCode", Constant.EmptyData);
                    data.put("statusMessage", "Incorrect Field");
                } else if (cardService.checkCartAccount(cardAccount)) {
                    if (cardService.editCartAccounts(cardAccount)) {
                        data.put("statusCode", Constant.SUCCESS);
                        data.put("statusMessage", "Success");
                    }
                } else {
                    data.put("statusCode", Constant.NotInformation);
                    data.put("statusMessage", "Not information");
                }

            } else {
                data.put("statusCode", Constant.WrongTransactionId);
                data.put("statusMessage", "Wrong TransactionId");
            }

        } catch (Exception ex) {
            data.put("statusCode", Constant.INTERNAL_EXCEPTION);
            data.put("statusMessage", "System Error");
            logger.info("error:"+ex);
        }

        logger.info("Request of CardEdit : " + GsonInitializer.getInstance().toJson(objectMap));
        logger.info("Response of CardEdit : " + GsonInitializer.getInstance().toJson(data));

        return data;
    }

    //delete bank account


    @RequestMapping(value = "/card/delete", method = RequestMethod.POST, consumes = "application/json")
    public Map<String, Object> deleteCard(@RequestBody Map<String,Object> objectMap) {
        Map<String, Object> data = new HashMap<>();
        Map<String, String> map;
        ObjectMapper mapper = new ObjectMapper();
        CardAccount cardAccount=new CardAccount();

        try {

            map = mapper.readValue(service.getHeadersInfo().get(Config.HEADER_KEY), new TypeReference<Map<String, String>>() {
            });
            cardAccount.setTransactionId(map.get(Config.TRANSACTION_KEY));
            User user = new User();
            user.setTransactionId(map.get(Config.TRANSACTION_KEY));


            cardAccount.setId(Integer.parseInt(objectMap.get("id").toString()));

            if (userService.checkUser(user)) {

                    if (cardService.checkCartAccount(cardAccount)) {
                        if (cardService.deleteCardAccounts(cardAccount)) {
                            data.put("statusCode", Constant.SUCCESS);
                            data.put("statusMessage", "Success");
                        }
                    } else {
                        data.put("statusCode", Constant.NotInformation);
                        data.put("statusMessage", "Not information");
                    }

            } else {
                data.put("statusCode", Constant.WrongTransactionId);
                data.put("statusMessage", "Wrong TransactionId");
            }

        } catch (Exception ex) {

            data.put("statusCode", Constant.INTERNAL_EXCEPTION);
            data.put("statusMessage", "System error");
            logger.info("error:"+ex);
        }

        logger.info("Request of CardDelete : " + GsonInitializer.getInstance().toJson(objectMap));
        logger.info("Response of CardDelete : " + GsonInitializer.getInstance().toJson(data));

        return data;


    }
}
