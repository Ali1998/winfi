package controller;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import domain.*;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import service.*;
import util.GsonInitializer;
import util.MethodService;
import util.Methods;

import java.util.HashMap;
import java.util.Map;
import java.util.List;


@RestController
public class PaymentController {


    @Autowired
    private UserService userService;

    @Autowired
    private ConfigService configService;

    @Autowired
    private ReportService reportService;

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private HttpService service;

    @Autowired
    private HttpClientService clientService;

    @Autowired
    private MethodService methodService;

    private static Logger logger=Logger.getLogger(PaymentController.class);



    @RequestMapping(value = "/payment/add", method = RequestMethod.POST, consumes = "application/json")
    public Map<String, Object> addPayment(@RequestBody Map<String,Object>objectMap) {
        Map<String, Object> data = new HashMap<>();
        Map<String, String> map;
        ObjectMapper mapper = new ObjectMapper();
        String bank="";
        String card="";

        try {
            map = mapper.readValue(service.getHeadersInfo().get(Config.HEADER_KEY), new TypeReference<Map<String, String>>() {
            });

            Configuration config=configService.getConfig();

            if(objectMap.containsKey("bankId")){
                bank=objectMap.get("bankId").toString().trim();
            }
            if(objectMap.containsKey("cardId")){
                card=objectMap.get("cardId").toString().trim();

            }

            Double amount=Double.parseDouble(objectMap.get("amount").toString());
            String transactionId=map.get(Config.TRANSACTION_KEY);
            Report totalEarning = reportService.getTotalEarning(transactionId);
            User user = new User();
            user.setTransactionId(transactionId);
            Operation success=reportService.getSuccessInfo(transactionId);
            Operation pendingInfo=reportService.getPendingInfo(transactionId);
            double paid=success.getAmount();
            double pending=pendingInfo.getAmount();

            if (userService.checkUser(user)) {


                if(bank.length() > 0 || card.length()>0){


                    if(bank.length() > 0 && card.length() > 0){
                        data.put("statusCode", Constant.InvalidRequest);
                        data.put("statusMessage", "Invalid Request");

                    } else {

                        Integer bankId = null;
                        Integer cardId=null;

                        if(bank.length()>0) {
                            bankId = Integer.parseInt(bank);

                        }

                        if(card.length()>0){
                            cardId=Integer.parseInt(card);
                        }


                        Double balance=totalEarning.getTotalEarning()-paid-pending;
                        if(paymentService.checkPaymentAccount(bankId,cardId,transactionId)){


                                if(amount>=config.getAmount()){
                                    if(amount<=balance){
                                        if(paymentService.addPayment(bankId,cardId, amount)){
                                            data.put("statusCode", Constant.SUCCESS);
                                            data.put("statusMessage", "success");
                                        }
                                    }else{
                                        data.put("statusCode", Constant.ERROR);
                                        data.put("statusMessage", "Your account does not have enough money");
                                    }

                                }
                                else{
                                    data.put("statusCode", Constant.WrongAmount);
                                    data.put("statusMessage", "Wrong Amount");
                                }



                        }else {
                            data.put("statusCode", Constant.NotInformation);
                            data.put("statusMessage", "Not Information");
                        }

                    }

                } else {
                    data.put("statusCode", Constant.EmptyData);
                    data.put("statusMessage", "Incorrect field");
                }

            } else {
                data.put("statusCode", Constant.WrongTransactionId);
                data.put("statusMessage", "Wrong TransactionId");
            }


        } catch (Exception ex) {

            data.put("statusCode", Constant.INTERNAL_EXCEPTION);
            data.put("statusMessage", "System error");
            logger.error("error" + ex);
        }

        logger.info("Request of PaymentAdd : " + GsonInitializer.getInstance().toJson(objectMap));
        logger.info("Response of PaymentAdd : " + GsonInitializer.getInstance().toJson(data));


        return data;
    }


    @RequestMapping(value = "/payment/getList", method = RequestMethod.POST, consumes = "application/json")
    public Map<String, Object> getTransactionList(@RequestBody Map<String,Object>objectMap) {
        Map<String, Object> data = new HashMap<>();
        Map<String, String> map;
        ObjectMapper mapper = new ObjectMapper();


        try {
            map = mapper.readValue(service.getHeadersInfo().get(Config.HEADER_KEY), new TypeReference<Map<String, String>>() {
            });

            User user = new User();
            user.setTransactionId(map.get(Config.TRANSACTION_KEY));
            String fromDate=objectMap.get("fromDate").toString();
            String toDate=objectMap.get("toDate").toString();
            Integer status=Integer.parseInt(objectMap.get("status").toString());
            String transactionId=map.get(Config.TRANSACTION_KEY);


            if (userService.checkUser(user)) {

                List<Transactions>transactions=paymentService.getTransactions(fromDate,toDate,status,transactionId);

                if(transactions==null || transactions.isEmpty()){
                    data.put("statusCode", Constant.NotInformation);
                    data.put("statusMessage", "Not Information");
                }else{
                    data.put("statusCode", Constant.SUCCESS);
                    data.put("Data", transactions);
                }


            } else {
                data.put("statusCode", Constant.WrongTransactionId);
                data.put("statusMessage", "Wrong TransactionId");
            }


        } catch (Exception ex) {

            data.put("statusCode", Constant.INTERNAL_EXCEPTION);
            data.put("statusMessage", "System error");
            logger.error("error" + ex);
        }

        logger.info("Request of getList : " + GsonInitializer.getInstance().toJson(objectMap));
        logger.info("Response of getList : " + GsonInitializer.getInstance().toJson(data));


        return data;
    }


//    @RequestMapping(value = "/payment/getPackagesList", method = RequestMethod.POST, consumes = "application/json")
//    public Map<String, Object> getPackagesList(@RequestBody Map<String,Object>objectMap) {
//        Map<String, Object> data = new HashMap<>();
//        Map<String, String> map;
//        ObjectMapper mapper = new ObjectMapper();
//
//
//        try {
//            map = mapper.readValue(service.getHeadersInfo().get(Config.HEADER_KEY), new TypeReference<Map<String, String>>() {
//            });
//
//
//            Integer id=Integer.parseInt(objectMap.get("countryId").toString());
//            String transactionId=map.get(Config.TRANSACTION_KEY);
//            User user=new User();
//            user.setTransactionId(transactionId);
//
//            if(!paymentService.checkConnectCountry(transactionId,id)){
//                List<Packages>packages=paymentService.getPackages(id);
//
//                if(packages==null || packages.isEmpty()){
//                    data.put("statusCode", Constant.NotInformation);
//                    data.put("statusMessage", "Not Information");
//                }else{
//                    data.put("statusCode", Constant.SUCCESS);
//                    data.put("packages", packages);
//                }
//            }else{
//                data.put("statusCode", Constant.CurrentData);
//                data.put("statusMessage", "you have a connect this country");
//            }
//
//
//
//        } catch (Exception ex) {
//
//            data.put("statusCode", Constant.INTERNAL_EXCEPTION);
//            data.put("statusMessage", "System error");
//            logger.error("error" + ex);
//        }
//
//
//        return data;
//    }


    @RequestMapping(value = "/payment/buyPackage", method = RequestMethod.POST, consumes = "application/json")
    public Map<String, Object> PackageBuy(@RequestBody Map<String,Object>objectMap) {
        Map<String, Object> data = new HashMap<>();
        Map<String, String> map;
        ObjectMapper mapper = new ObjectMapper();


        try {
            map = mapper.readValue(service.getHeadersInfo().get(Config.HEADER_KEY), new TypeReference<Map<String, String>>() {
            });

            User user = new User();
            String transactionId=map.get(Config.TRANSACTION_KEY);
            user.setTransactionId(transactionId);
            Integer id=Integer.parseInt(objectMap.get("packId").toString());
            String psp_rrn=objectMap.get("psp_rrn").toString();
            RequestDomain requestDomain=new RequestDomain();
            requestDomain.setPsp_rrn(psp_rrn);
            String url="https://psp.mps.az/check";

            if (userService.checkUser(user)) {

                PackagesBuy packages=paymentService.getPackageById(id);

                String res=clientService.sendPost(url,requestDomain);

                JSONObject json = new JSONObject(res);

                Integer statusCode=Integer.parseInt(json.get("code").toString());
               if(statusCode==0){
                   if(packages!=null ){
                       if(paymentService.connectCountry(packages,transactionId)){
                           data.put("statusCode", Constant.SUCCESS);
                           data.put("statusMessage", "Success!");
                       }
                   }else{
                       data.put("statusCode", Constant.NotInformation);
                       data.put("statusMessage", "this package not found!");
                   }
               }
               else if(statusCode==1){
                   data.put("statusCode", Constant.PaymentStatus);
                   data.put("statusMessage", "this is pending! Please try again");
               }else{
                   data.put("statusCode", Constant.InvalidRequest);
                   data.put("statusMessage", "Request Failed");
               }


            } else {
                data.put("statusCode", Constant.WrongTransactionId);
                data.put("statusMessage", "Wrong TransactionId");
            }


        } catch (Exception ex) {

            data.put("statusCode", Constant.INTERNAL_EXCEPTION);
            data.put("statusMessage", "System error");
            ex.printStackTrace();
            logger.error("error" + ex);
        }


        logger.info("Request of buyPackage : " + GsonInitializer.getInstance().toJson(objectMap));
        logger.info("Response of buyPackage : " + GsonInitializer.getInstance().toJson(data));


        return data;
    }



    @RequestMapping(value = "/payment/packageBuy", method = RequestMethod.POST, consumes = "application/json")
    public Map<String, Object> BuyPackage(@RequestBody Map<String,Object>objectMap) {
        Map<String, Object> data = new HashMap<>();
        Map<String, String> map;
        ObjectMapper mapper = new ObjectMapper();


        try {
            map = mapper.readValue(service.getHeadersInfo().get(Config.HEADER_KEY), new TypeReference<Map<String, String>>() {
            });

            User user = new User();
            String transactionId=map.get(Config.TRANSACTION_KEY);
            user.setTransactionId(transactionId);
            Integer id=Integer.parseInt(objectMap.get("packId").toString());

            if (userService.checkUser(user)) {

                PackagesBuy packages=paymentService.getPackageById(id);
                    if(packages!=null ){
                        if(paymentService.connectCountry(packages,transactionId)){
                            data.put("statusCode", Constant.SUCCESS);
                            data.put("statusMessage", "Success!");
                        }
                    }else{
                        data.put("statusCode", Constant.NotInformation);
                        data.put("statusMessage", "This package not found!");
                    }


            } else {
                data.put("statusCode", Constant.WrongTransactionId);
                data.put("statusMessage", "Wrong TransactionId");
            }


        } catch (Exception ex) {

            data.put("statusCode", Constant.INTERNAL_EXCEPTION);
            data.put("statusMessage", "System error");
            ex.printStackTrace();
            logger.error("error" + ex);
        }

        logger.info("Request of packageBuy : " + GsonInitializer.getInstance().toJson(objectMap));
        logger.info("Response of packageBuy : " + GsonInitializer.getInstance().toJson(data));

        return data;
    }





    @RequestMapping(value = "/payment/getPackagesList", method = RequestMethod.POST, consumes = "application/json")
    public Map<String, Object> getPackagesList(@RequestBody Map<String,Object>objectMap) {
        Map<String, Object> data = new HashMap<>();
        Map<String, String> map;
        ObjectMapper mapper = new ObjectMapper();
        List<Packages>packages;


        try {
            map = mapper.readValue(service.getHeadersInfo().get(Config.HEADER_KEY), new TypeReference<Map<String, String>>() {
            });


            Integer id=Integer.parseInt(objectMap.get("countryId").toString());
            String transactionId=map.get(Config.TRANSACTION_KEY);
            User user=new User();
            user.setTransactionId(transactionId);


            if(userService.checkUser(user)){

                if(!paymentService.checkConnectCountry(transactionId,id)){

                    if(methodService.checkType(id,transactionId)){
                        packages= paymentService.getPackages(id,1);
                    }else{
                        packages=paymentService.getPackages(id,2);
                    }



                    if(packages==null || packages.isEmpty()){
                        data.put("statusCode", Constant.NotInformation);
                        data.put("statusMessage", "No Information");
                    }else{
                        data.put("statusCode", Constant.SUCCESS);
                        data.put("packages", packages);
                    }
                }else{
                    data.put("statusCode", Constant.CurrentData);
                    data.put("statusMessage", "you have a connect this country");
                }
            }else{
                data.put("statusCode",Constant.WrongTransactionId);
                data.put("statusMessage","Wrong TransactionId");
            }





        } catch (Exception ex) {

            data.put("statusCode", Constant.INTERNAL_EXCEPTION);
            data.put("statusMessage", "System error");
            ex.printStackTrace();
            logger.error("error" + ex);
        }
        logger.info("Request of getPackagesList : " + GsonInitializer.getInstance().toJson(objectMap));
        logger.info("Response of getPackagesList : " + GsonInitializer.getInstance().toJson(data));


        return data;
    }




    @RequestMapping(value = "/callBack",produces = "application/json")
    public @ResponseBody Integer callBack() {
        System.out.println("1");
        logger.info("callback url works:::");

        return 1;
    }






}
