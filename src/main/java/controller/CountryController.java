package controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import domain.*;
import listener.CurrencyListener;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import service.*;
import util.GsonInitializer;
import util.Methods;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.MemoryHandler;

@RestController
public class CountryController {


    @Autowired
    private CountryService countryService;

    @Autowired
    private HttpService service;

    @Autowired
    private UserService userService;

    @Autowired
    private CurrencyService currencyService;

    @Autowired
    private ConfigService configService;




    private static final Logger logger = Logger.getLogger(CountryController.class);

    @RequestMapping(value = "/country/getList", method = RequestMethod.GET, produces = "application/json")
    public Map<String, Object> getCountryList() {

        Map<String, Object> data = new HashMap<>();

        if(!Config.CONFIG_RUN){

            CurrencyListener.start();

            Config.CONFIG_RUN=true;

        }


        try {

                List<Country> countryList = countryService.getCountryList();

                if (countryList.isEmpty() || countryList == null) {
                    data.put("statusCode", Constant.NotInformation);
                    data.put("statusMessage", "Not Information");
                } else {
                    data.put("statusCode", Constant.SUCCESS);
                    data.put("countries", countryList);

                }

        } catch (Exception ex) {
            data.put("statusCode", Constant.INTERNAL_EXCEPTION);
            data.put("statusMessage", "System Error");
            logger.info("error:"+ex);
        }

        return data;

    }




    @RequestMapping(value = "/country/getWifiList", method = RequestMethod.GET, produces = "application/json")
    public Map<String, Object> getCountryWifiList(@RequestParam(value = "id") String id) {

        Map<String, Object> data = new HashMap<>();
        try {



            if(id.length()>0 && Methods.isNumber(id)){
                List<CountryWifi> countryWifis = countryService.getCountryWifiList(Long.parseLong(id));

                if (countryWifis.isEmpty() || countryWifis == null) {
                    data.put("statusCode", Constant.NotInformation);
                    data.put("statusMessage", "Not Information");
                } else {
                    data.put("statusCode", Constant.SUCCESS);
                    data.put("wifi", countryWifis);

                }
            }
            else{
                data.put("statusCode", Constant.InvalidValue);
                data.put("statusMessage", "Invalid Type");
            }



        } catch (Exception ex) {
            data.put("statusCode", Constant.INTERNAL_EXCEPTION);
            data.put("statusMessage", "System Error");
            logger.info("error:"+ex);
        }

        logger.info("Request of GetWifiList : " + GsonInitializer.getInstance().toJson(id));
        logger.info("Response of GetWifiList : " + GsonInitializer.getInstance().toJson(data));

        return data;

    }


    @RequestMapping(value = "/report/checkCountry", method = RequestMethod.POST, produces = "application/json")
    public Map<String, Object> getCheckCountry(@RequestBody Map<String,Object>objectMap) {

        Map<String, Object> data = new HashMap<>();
        Map<String, String> map;
        ObjectMapper mapper = new ObjectMapper();
        Country country=new Country();
        Boolean result;
        try {

            map = mapper.readValue(service.getHeadersInfo().get(Config.HEADER_KEY), new TypeReference<Map<String, String>>() {
            });
            User user = new User();
            String transactionId = map.get(Config.TRANSACTION_KEY);
            user.setTransactionId(transactionId);
            country.setId(Long.parseLong(objectMap.get("id").toString()));



            if (userService.checkUser(user)) {



                if(countryService.currentCountry(country))
                {
                    if (countryService.checkCountry(transactionId, country)) {
                        result = false;
                        data.put("statusCode", Constant.SUCCESS);
                        data.put("statusMessage", "Success");
                        data.put("paymentNeed", result);
                    } else {
                        result = true;
                        data.put("statusCode", Constant.SUCCESS);
                        data.put("statusMessage", "Success");
                        data.put("paymentNeed", result);
                    }
                }else{
                    data.put("statusCode", Constant.NotInformation);
                    data.put("statusMessage", "This country was not found or not wifi in country!");
                }

            } else {
                data.put("statusCode", Constant.WrongTransactionId);
                data.put("statusMessage", "Wrong TransactionId");
            }


        } catch (Exception ex) {
            data.put("statusCode", Constant.INTERNAL_EXCEPTION);
            data.put("statusMessage", "System Error");
            logger.info("error:"+ex);
        }

        logger.info("Request of CheckCountry : " + GsonInitializer.getInstance().toJson(objectMap));
        logger.info("Response of CheckCountry : " + GsonInitializer.getInstance().toJson(data));

        return data;

    }


    @RequestMapping(value = "/report/ownedCountry", method = RequestMethod.POST, produces = "application/json")
    public Map<String, Object> getOwnedCountryList(HttpServletResponse response) {

        Map<String, Object> data = new HashMap<>();
        Map<String, String> map;
        ObjectMapper mapper = new ObjectMapper();
        Map<String,String>android;
        String androidKey=null;
        String key=Methods.getSecurityKey();
        String initVector=Methods.getSecurityKey();
        Cryptography cryptography=new Cryptography();

        try {
            map = mapper.readValue(service.getHeadersInfo().get(Config.HEADER_KEY), new TypeReference<Map<String, String>>() {
            });
            User user = new User();
            String transactionId = map.get(Config.TRANSACTION_KEY);
            user.setTransactionId(transactionId);
            android=service.getHeadersInfo();
            if(android.get("android")!=null){
                androidKey=android.get("android");
            }

            if (userService.checkUser(user)) {
                List<OwnedCountries> list = countryService.getOwnedCountry(transactionId);
                if (list.isEmpty() || list == null) {
                    data.put("statusCode", Constant.NotInformation);
                    data.put("statusMessage", "Not Information");
                } else {

                    if(androidKey!=null){
                        if(androidKey.equals("1")){

                            cryptography.setInitVector(initVector);
                            cryptography.setKey(key);
                            for(OwnedCountries ownedCountries:list){
                                List<OwnedWifi>wifies=ownedCountries.getWifi();
                                for(OwnedWifi wifi:wifies){
                                    wifi.setPassword(Methods.encrypt(wifi.getPassword(),cryptography));
                                }
                            }

                            data.put("ownedCountries", list);
                            data.put("statusCode",Constant.SUCCESS);
                            response.addHeader("initVector",initVector);
                            response.addHeader("key",key);
                       }


                    }else{

                        data.put("ownedCountries", list);
                        data.put("statusCode",Constant.SUCCESS);
                    }


                }

            } else {
                data.put("statusCode", Constant.WrongTransactionId);
                data.put("statusMessage", "Wrong TransactionId");
            }


        } catch (Exception ex) {
            data.put("statusCode", Constant.INTERNAL_EXCEPTION);
            data.put("statusMessage", "System Error");
            logger.info("error:"+ex);
        }

        return data;

    }










}
