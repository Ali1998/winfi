package controller;


import java.util.*;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import domain.*;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import service.*;
import util.GsonInitializer;


@RestController
public class WifiController {


    @Autowired
    private WifiService wifiService;
    @Autowired
    private HttpService service;
    @Autowired
    private UserService userService;
    @Autowired
    private CountryService countryService;

    @Autowired
    private ConfigService configService;

    @Autowired
    private HttpClientService clientService;




    private static final Logger logger = Logger.getLogger(WifiController.class);


    @RequestMapping(value = "/wifi/getList", method = RequestMethod.POST, produces = "application/json")
    public Map<String, Object> getWifiList() {

        Map<String, Object> data = new HashMap<>();
        Map<String, String> map;
        ObjectMapper mapper = new ObjectMapper();


        try {
            map = mapper.readValue(service.getHeadersInfo().get(Config.HEADER_KEY), new TypeReference<Map<String, String>>() {
            });


            User user = new User();
            user.setTransactionId(map.get(Config.TRANSACTION_KEY));

            if (userService.checkUser(user)) {
                List<ResponseWifi> wifiLists = wifiService.getWifiList(map.get(Config.TRANSACTION_KEY));
                if (wifiLists != null && !wifiLists.isEmpty()) {
                    data.put("wifi", wifiLists);
                    data.put("statusCode", Constant.SUCCESS);
                } else {
                    data.put("statusCode", Constant.NotInformation);
                    data.put("statusMessage", "Not information");
                }
            } else {
                data.put("statusCode", Constant.WrongTransactionId);
                data.put("statusMessage", "Wrong TransactionId");
            }


        } catch (Exception ex) {
            data.put("statusCode", Constant.INTERNAL_EXCEPTION);
            data.put("statusMessage", "System Error");
            logger.info("error:"+ex);
        }
        return data;

    }


    ////Add Wifi

    @RequestMapping(value = "/wifi/add", method = RequestMethod.POST, consumes = "application/json")
    public Map<String, Object> addWifi(@RequestBody Wifies wifies) {
        Map<String, Object> data = new HashMap<>();
        Map<String, String> map;
        ObjectMapper mapper = new ObjectMapper();
        String name = wifies.getName();
        try {
            map = mapper.readValue(service.getHeadersInfo().get(Config.HEADER_KEY), new TypeReference<Map<String, String>>() {
            });
            User user = new User();
            user.setTransactionId(map.get(Config.TRANSACTION_KEY));
            wifies.setTransactionId(map.get(Config.TRANSACTION_KEY));


            if (userService.checkUser(user)) {

                if(wifies.getIp()!=null && wifies.getIp().trim().length()>0){
                    String url="https://ipinfo.io/"+wifies.getIp()+"/json";
                    if(wifies.getCountryCode()==null || wifies.getCountryCode().trim().length()==0){
                        String res=clientService.sendGet(url);
                        JSONObject json = new JSONObject(res);
                        String countryCode=String.valueOf(json.get("country"));
                        wifies.setCountryCode(countryCode);
                    }
                }
                if(!wifiService.currentCountryCode(wifies.getCountryCode())){
                    wifies.setCountryCode("undefined");
                }


                if ((wifies.getPassword() == null || wifies.getPassword().trim().length() < 5 ||
                        wifies.getName() == null ||  wifies.getName().trim().length()==0 ||
                        wifies.getLocation() == null || wifies.getLocation().trim().length()==0 ||
                        wifies.getAddress() == null ||  wifies.getAddress().trim().length()==0 ||
                        wifies.getMacAddress() == null || wifies.getMacAddress().trim().length()==0)) {

                    data.put("statusCode", Constant.EmptyData);
                    data.put("statusMessage", "Incorrect field");
                }else if (wifiService.checkWifi(name,wifies.getPassword(),0)) {
                    data.put("statusCode", Constant.CurrentData);
                    data.put("statusMessage", "Already Added. If you are owner this WiFi, then please reset password of WiFi.");
                }else {
                    if (wifiService.addWifi(wifies)) {
                        data.put("statusCode", Constant.SUCCESS);
                        data.put("statusMessage", "Success");
                    }
                }


            } else {
                data.put("statusCode", Constant.WrongTransactionId);
                data.put("statusMessage", "Wrong TransactionId");
            }


        } catch (Exception ex) {

            data.put("statusCode", Constant.INTERNAL_EXCEPTION);
            data.put("statusMessage", "System Error");
            logger.info("error:"+ex);
        }


        logger.info("Request of WifiAdd : "+GsonInitializer.getInstance().toJson(wifies));
        logger.info("Response of WifiAdd : " + GsonInitializer.getInstance().toJson(data));

        return data;
    }


    ///edit wifi


    @RequestMapping(value = "/wifi/edit", method = RequestMethod.POST, consumes = "application/json")
    public Map<String, Object> editWifi(@RequestBody Map<String,Object>objectMap ) {
        Map<String, Object> data = new HashMap<>();
        Map<String, String> map;
        ObjectMapper mapper = new ObjectMapper();
        Wifies wifies =new Wifies();

        try {
            map = mapper.readValue(service.getHeadersInfo().get(Config.HEADER_KEY), new TypeReference<Map<String, String>>() {
            });
            wifies.setTransactionId(map.get(Config.TRANSACTION_KEY));
            User user = new User();
            user.setTransactionId(map.get(Config.TRANSACTION_KEY));
            wifies.setId(Integer.parseInt(objectMap.get("id").toString()));
            wifies.setAddress(objectMap.get("address").toString());
            wifies.setName(objectMap.get("name").toString());
            wifies.setPassword(objectMap.get("password").toString());
            wifies.setLocation(objectMap.get("location").toString());
            wifies.setDescription(objectMap.get("description").toString());
            String name = wifies.getName();
            String password=wifies.getPassword();


            if (userService.checkUser(user)) {
                if ((wifies.getPassword() == null || wifies.getPassword().trim().length() < 5 ||
                        wifies.getName() == null ||  wifies.getName().trim().length()==0 ||
                        wifies.getLocation() == null || wifies.getLocation().trim().length()==0 ||
                        wifies.getAddress() == null ||  wifies.getAddress().trim().length()==0)) {

                    data.put("statusCode", Constant.EmptyData);
                    data.put("statusMessage", "Incorrect field");
                } else if (wifiService.checkWifi(wifies)) {

                    if(wifiService.checkWifi(name,password,wifies.getId())){
                        data.put("statusCode", Constant.CurrentData);
                        data.put("statusMessage", "This wifi already added!");
                    }else{
                        if (wifiService.editWifi(wifies)) {
                            data.put("statusCode", Constant.SUCCESS);
                            data.put("statusMessage", "Success");
                        }
                    }

                } else {
                    data.put("statusCode", Constant.NotInformation);
                    data.put("statusMessage", "Not information");
                }

            } else {
                data.put("statusCode", Constant.WrongTransactionId);
                data.put("statusMessage", "Wrong TransactionId");
            }

        } catch (Exception ex) {
            data.put("statusCode", Constant.INTERNAL_EXCEPTION);
            data.put("statusMessage", "System error");
            logger.info("error:"+ex);
        }

        logger.info("Request of WifiEdit : " + GsonInitializer.getInstance().toJson(objectMap));
        logger.info("Response of WifiEdit : " + GsonInitializer.getInstance().toJson(data));

        return data;
    }


    //delete Wifi

    @RequestMapping(value = "/wifi/delete", method = RequestMethod.POST, consumes = "application/json")
    public Map<String, Object> deleteWifi(@RequestBody Map<String,Object>objectMap) {


        Map<String, Object> data = new HashMap<>();
        Map<String, String> map;
        ObjectMapper mapper = new ObjectMapper();
        Wifies wifies =new Wifies();
        try {
            map = mapper.readValue(service.getHeadersInfo().get(Config.HEADER_KEY), new TypeReference<Map<String, String>>() {
            });
            wifies.setTransactionId(map.get(Config.TRANSACTION_KEY));
            User user = new User();
            user.setTransactionId(map.get(Config.TRANSACTION_KEY));
            wifies.setId(Integer.parseInt(objectMap.get("id").toString()));

            if (userService.checkUser(user)) {

                 if (wifiService.checkWifi(wifies)) {
                    if (wifiService.deleteWifi(wifies)) {
                        data.put("statusCode", Constant.SUCCESS);
                        data.put("statusMessage", "Success");
                    }
                } else {
                    data.put("statusCode", Constant.NotInformation);
                    data.put("statusMessage", "Not information");
                }
            } else {
                data.put("statusCode", Constant.WrongTransactionId);
                data.put("statusMessage", "Wrong TransactionId");
            }


        } catch (Exception ex) {

            data.put("statusCode", Constant.INTERNAL_EXCEPTION);
            data.put("statusMessage", "System Error");
            logger.info("error:"+ex);
        }

        logger.info("Request of WifiDelete : " + GsonInitializer.getInstance().toJson(objectMap));
        logger.info("Response of WifiDelete : " + GsonInitializer.getInstance().toJson(data));

        return data;

    }



    //user

    @RequestMapping(value = "/wifi/connect", method = RequestMethod.POST, produces = "application/json")
    public Map<String, Object> getCountryList(@RequestBody Map<String,Object>objectMap) {
        Map<String, Object> data = new HashMap<>();
        Map<String, String> map;
        ObjectMapper mapper = new ObjectMapper();
        Wifies wifies =new Wifies();

        try {
            map = mapper.readValue(service.getHeadersInfo().get(Config.HEADER_KEY), new TypeReference<Map<String, String>>() {
            });
            String transactionId=map.get(Config.TRANSACTION_KEY);
            User user = new User();
            user.setTransactionId(transactionId);
            wifies.setId(Integer.parseInt(objectMap.get("id").toString()));
            wifies.setTransactionId(transactionId);
            Configuration configuration=configService.getConfig();

            if(userService.checkUser(user))
            {
                if(wifiService.currentWifi(wifies)){
                    if(countryService.checkCountryWifi(transactionId, wifies.getId())){
                        if(wifiService.checkWifi(wifies)){
                            data.put("statusCode",Constant.SUCCESS);
                            data.put("statusMessage","Success!");
                        }

                        else if(wifiService.addConnect(wifies,transactionId,configuration.getCost()))
                        {
                            data.put("statusCode",Constant.SUCCESS);
                            data.put("statusMessage","Success!");
                        }
                    }else if(countryService.checkCountryWifiConnect(transactionId, wifies.getId())){
                        data.put("statusCode", Constant.SESSION_END);
                        data.put("statusMessage", "this country connect date end");
                    }else {
                        data.put("statusCode", Constant.ERROR);
                        data.put("statusMessage", "this country no connect");
                    }
                }else{
                    data.put("statusCode", Constant.ERROR);
                    data.put("statusMessage", "this wifi not found");
                }

            }

            else
            {
                data.put("statusCode", Constant.WrongTransactionId);
                data.put("statusMessage", "Wrong TransactionId");
            }

        } catch (Exception ex) {
            data.put("statusCode", Constant.INTERNAL_EXCEPTION);
            data.put("statusMessage", "System Error");
            logger.info("error:"+ex);
        }

        logger.info("Request of WifiConnect : " + GsonInitializer.getInstance().toJson(objectMap));
        logger.info("Response of WifiConnect : " + GsonInitializer.getInstance().toJson(data));

        return data;

    }

}
