package controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import domain.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.*;
import service.BankService;
import service.HttpService;
import service.UserService;
import util.GsonInitializer;
import util.Methods;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class BankController {


    private static final Logger logger = Logger.getLogger(BankController.class);


    @Autowired
    private BankService bankService;

    @Autowired
    private HttpService service;
    @Autowired
    private UserService userService;

    @RequestMapping(value = "/bank/getList", method = RequestMethod.POST, produces = "application/json")
    public Map<String, Object> getWifiList() {


        Map<String, Object> data = new HashMap<>();
        Map<String, String> map;
        ObjectMapper mapper = new ObjectMapper();
        try {


            map = mapper.readValue(service.getHeadersInfo().get(Config.HEADER_KEY), new TypeReference<Map<String, String>>() {
            });
            User user = new User();
            user.setTransactionId(map.get(Config.TRANSACTION_KEY));


            if (userService.checkUser(user)) {
                List<ResponseBankAccounts> bankAccounts = bankService.getBankList(map.get(Config.TRANSACTION_KEY));
                if (bankAccounts != null && !bankAccounts.isEmpty()) {
                    data.put("statusCode", Constant.SUCCESS);
                    data.put("accounts", bankAccounts);
                } else {
                    data.put("statusCode", Constant.NotInformation);
                    data.put("statusMessage", "not information");
                }

            } else {
                data.put("statusCode", Constant.WrongTransactionId);
                data.put("statusMessage", "Wrong TransactionId");
            }


        } catch (Exception ex) {
            data.put("statusCode", Constant.INTERNAL_EXCEPTION);
            data.put("statusMessage", "System Error");
            logger.error("error" + ex);
        }

        return data;

    }


    //ADD BANK

    @RequestMapping(value = "/bank/add", method = RequestMethod.POST, consumes = "application/json")
    public Map<String, Object> addBankAccount(@RequestBody BankAccounts bankAccounts) {
        Map<String, Object> data = new HashMap<>();
        Map<String, String> map;
        ObjectMapper mapper = new ObjectMapper();


        try {
            map = mapper.readValue(service.getHeadersInfo().get(Config.HEADER_KEY), new TypeReference<Map<String, String>>() {
            });
            User user = new User();
            user.setTransactionId(map.get(Config.TRANSACTION_KEY));
            bankAccounts.setTransactionId(map.get(Config.TRANSACTION_KEY));
            if (userService.checkUser(user)) {
                if (bankAccounts.getIban() == null || bankAccounts.getIban().trim().length()<16 ||
                        bankAccounts.getName() == null || bankAccounts.getName().trim().length()==0) {
                    data.put("statusCode", Constant.EmptyData);
                    data.put("statusMessage", "incorrect field");
                } else if (bankService.addBankAccounts(bankAccounts)) {
                    data.put("statusCode", Constant.SUCCESS);
                    data.put("statusMessage", "Success");
                }
            } else {
                data.put("statusCode", Constant.WrongTransactionId);
                data.put("statusMessage", "Wrong TransactionId");
            }


        } catch (Exception ex) {

            data.put("statusCode", Constant.INTERNAL_EXCEPTION);
            data.put("statusMessage", "System error");
            logger.error("error" + ex);
        }

        logger.info("Response of BankAdd : " + GsonInitializer.getInstance().toJson(data));


        return data;
    }

    //Edit Bank

    @RequestMapping(value = "/bank/edit", method = RequestMethod.POST, consumes = "application/json")
    public Map<String, Object> editWifi(@RequestBody Map<String,Object>objectMap) {
        Map<String, Object> data = new HashMap<>();
        Map<String, String> map;
        ObjectMapper mapper = new ObjectMapper();
        BankAccounts bankAccounts=new BankAccounts();
        try {
            map = mapper.readValue(service.getHeadersInfo().get(Config.HEADER_KEY), new TypeReference<Map<String, String>>() {
            });
            bankAccounts.setTransactionId(map.get(Config.TRANSACTION_KEY));
            User user = new User();
            user.setTransactionId(map.get(Config.TRANSACTION_KEY));
            bankAccounts.setId(Integer.parseInt(objectMap.get("id").toString()));
            bankAccounts.setIban(objectMap.get("iban").toString());
            bankAccounts.setName(objectMap.get("name").toString());

            if (userService.checkUser(user)) {
                if ((bankAccounts.getName() == null || bankAccounts.getName().trim().length()==0 ||
                        bankAccounts.getIban() == null || bankAccounts.getIban().trim().length()<16)) {
                    data.put("statusCode", Constant.EmptyData);
                    data.put("statusMessage", "Incorrect field");
                } else if (bankService.checkBankAccount(bankAccounts)) {
                    if (bankService.editBankAccounts(bankAccounts)) {
                        data.put("statusCode", Constant.SUCCESS);
                        data.put("statusMessage", "Success");
                    }
                } else {
                    data.put("statusCode", Constant.NotInformation);
                    data.put("statusMessage", "Not information");
                }
            } else {
                data.put("statusCode", Constant.WrongTransactionId);
                data.put("statusMessage", "Wrong TransactionId");
            }


        } catch (Exception ex) {

            data.put("statusCode", Constant.INTERNAL_EXCEPTION);
            data.put("statusMessage", "System Error");
            logger.error("error" + ex);

        }

        logger.info("Request of BankEdit : " + GsonInitializer.getInstance().toJson(objectMap));
        logger.info("Response of BankEdit : " + GsonInitializer.getInstance().toJson(data));
        return data;
    }

    //delete bank account

    @RequestMapping(value = "/bank/delete", method = RequestMethod.POST, consumes = "application/json")
    public Map<String, Object> deleteWifi(@RequestBody Map<String,Object>objectMap) {
        Map<String, Object> data = new HashMap<>();
        Map<String, String> map;
        ObjectMapper mapper = new ObjectMapper();
        BankAccounts bankAccounts=new BankAccounts();
        try {


            map = mapper.readValue(service.getHeadersInfo().get(Config.HEADER_KEY), new TypeReference<Map<String, String>>() {
            });
            bankAccounts.setTransactionId(map.get(Config.TRANSACTION_KEY));
            User user = new User();
            user.setTransactionId(map.get(Config.TRANSACTION_KEY));

            bankAccounts.setId(Integer.parseInt(objectMap.get("id").toString()));


            if (userService.checkUser(user)) {

                   if (bankService.checkBankAccount(bankAccounts)) {
                        if (bankService.deleteBankAccounts(bankAccounts)) {
                            data.put("statusCode", Constant.SUCCESS);
                            data.put("statusMessage", "Success");
                        }
                    } else {
                        data.put("statusCode", Constant.NotInformation);
                        data.put("statusMessage", "Not information");
                    }

            } else {
                data.put("statusCode", Constant.WrongTransactionId);
                data.put("statusMessage", "Wrong TransactionId");
            }

        } catch (Exception ex) {

            data.put("statusCode", Constant.INTERNAL_EXCEPTION);
            data.put("statusMessage", "System Error");
            logger.error("error" + ex);

            ex.printStackTrace();
        }

        logger.info("Request of BankDelete : " + GsonInitializer.getInstance().toJson(objectMap));
        logger.info("Response of BankDelete : " + GsonInitializer.getInstance().toJson(data));


        return data;

    }


}
