package listener;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import service.CurrencyService;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Service
public class CurrencyListener {


    private static final Logger logger = Logger.getLogger(CurrencyListener.class);

    private static final ScheduledExecutorService executorService = Executors.newScheduledThreadPool(1);

    private static final CurrencyService currencyService=new CurrencyService();

    public static void start() {
        logger.info("Starting Listener...");

        executorService.scheduleAtFixedRate(new Runnable() {
                                                @Override
                                                public void run() {
                                                    logger.info("Thread starting...");

                                                    currencyService.currency(2);

                                                    logger.info("Valuta update!");

                                                }
                                            },
                0, 1, TimeUnit.MINUTES);
    }







}
