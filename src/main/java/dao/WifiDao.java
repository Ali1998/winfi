package dao;

import java.util.List;

import domain.ResponseWifi;
import domain.Wifies;

import javax.sql.DataSource;

public interface WifiDao {

    void SetJdbC(DataSource jdbc);

    List<ResponseWifi>getWifiList(String transactionId);

    boolean addWifi(Wifies wifies);

    boolean checkWifi(Wifies wifies);

    boolean editWifi(Wifies wifies);

    boolean deleteWifi(Wifies wifies);

    boolean checkWifi(String name,String password,Integer id);

    boolean addConnect(Wifies wifies, String transactionId, double price);

    boolean currentWifi(Wifies wifi);

    boolean currentCountryCode(String countryCode);








}
