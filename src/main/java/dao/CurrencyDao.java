package dao;



import javax.sql.DataSource;
import java.util.Map;

public interface CurrencyDao {

    void setJdbc(DataSource dataSource);

    boolean addCurrency(Map<String,Object> map);

    boolean updateCurrency(Map<String,Object>map);



}
