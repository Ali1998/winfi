package dao.impl;

import dao.PaymentDao;
import dao.SqlQueries;
import domain.Packages;
import domain.PackagesBuy;
import domain.Transactions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import javax.sql.DataSource;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;


@Repository
public class PaymentDaoImpl implements PaymentDao {

    private NamedParameterJdbcTemplate jdbcTemplate;
    DateFormat df=new SimpleDateFormat("yyyy-MM-dd");

    @Autowired
    @Qualifier("dataSource")
    @Override
    public void setJdbc(DataSource jdbc) {
       this.jdbcTemplate=new NamedParameterJdbcTemplate(jdbc);
    }

    @Override
    public boolean addPayment(Integer bankId, Integer cardId, Double amount) {
        MapSqlParameterSource param=new MapSqlParameterSource();
        param.addValue("bankId",bankId);
        param.addValue("cardId",cardId);
        param.addValue("amount",amount);
        return jdbcTemplate.update(SqlQueries.ADD_PAYMENT,param)>0;
    }

    @Override
    public boolean checkPaymentAccount(Integer bankId, Integer cardId, String transactionId) {
        MapSqlParameterSource param=new MapSqlParameterSource();
        param.addValue("transactionId",transactionId);
        if(bankId!=null){
            param.addValue("bankId",bankId);
            return jdbcTemplate.queryForObject(SqlQueries.CHECK_BANK_INFORMATION,param,Integer.class)>0;
        }
        else{
            param.addValue("cardId",cardId);
            return jdbcTemplate.queryForObject(SqlQueries.CHECK_CARD_INFORMATION,param,Integer.class)>0;
        }

    }

    @Override
    public List<Transactions> getTransactions(String fromDate, String toDate, Integer status,String transactionId) throws Exception {

            MapSqlParameterSource param=new MapSqlParameterSource();
            param.addValue("status",status);
            param.addValue("transactionId",transactionId);
            String sql=SqlQueries.GET_TRANSACTIONS_LIST;
            if(fromDate!=null && fromDate.trim().length()>0){
                sql+=" and p.create_date>='"+new java.sql.Date(df.parse(fromDate).getTime())+"'";
            }
            if(toDate!=null && toDate.trim().length()>0){
                Calendar c = Calendar.getInstance();
                c.setTime(df.parse(toDate));
                c.add(Calendar.DATE,1);
                toDate=df.format(c.getTime());
                sql+=" and p.create_date<'"+new java.sql.Date(df.parse(toDate).getTime())+"'";
            }
            if(status!=3){
                sql+=" and p.status="+status+"";
            }
            return jdbcTemplate.query(sql,param,BeanPropertyRowMapper.newInstance(Transactions.class));



    }

    @Override
    public List<Packages>getPackages(long id,int type) {

        MapSqlParameterSource param=new MapSqlParameterSource();
        param.addValue("countryId",id);
        param.addValue("type",type);
        return jdbcTemplate.query(SqlQueries.GET_PACKAGES,param,BeanPropertyRowMapper.newInstance(Packages.class));
    }

    @Override
    public PackagesBuy getPackageById(long id) {
        MapSqlParameterSource param=new MapSqlParameterSource();
        param.addValue("packId",id);
        return jdbcTemplate.queryForObject(SqlQueries.GET_PACKAGES_BY_ID,param,BeanPropertyRowMapper.newInstance(PackagesBuy.class));
    }

    @Override
    public boolean connectCountry(PackagesBuy p, String transactionId) {
        MapSqlParameterSource param=new MapSqlParameterSource();
        param.addValue("countryId",p.getCountryId());
        param.addValue("duration",p.getDuration());
        param.addValue("transactionId",transactionId);
        param.addValue("price",p.getPrice());
        param.addValue("packId",p.getPackId());
        return jdbcTemplate.update(SqlQueries.JOIN_COUNTRY,param)>0;
    }

    @Override
    public boolean checkConnectCountry(String transactionId, Integer countryId) {
        MapSqlParameterSource param=new MapSqlParameterSource();
        param.addValue("transactionId",transactionId);
        param.addValue("countryId",countryId);
        return jdbcTemplate.queryForObject(SqlQueries.CHECK_CONNECT_COUNTRY,param,Integer.class)>0;
    }





}
