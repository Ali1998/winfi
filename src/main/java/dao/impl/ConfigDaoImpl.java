package dao.impl;


import dao.ConfigDao;

import dao.SqlQueries;
import domain.Config;
import domain.Configuration;
import domain.Cryptography;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.object.SqlQuery;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;


@Repository
public class ConfigDaoImpl implements ConfigDao {

    private NamedParameterJdbcTemplate jdbcTemplate;


    @Autowired
    @Qualifier("dataSource")
    @Override
    public void setJdbc(DataSource jdbc) {
        this.jdbcTemplate=new NamedParameterJdbcTemplate(jdbc);
    }

    @Override
    public Configuration getConfig() {
        return jdbcTemplate.queryForObject(SqlQueries.SELECT_CONFIG,new MapSqlParameterSource(), BeanPropertyRowMapper.newInstance(Configuration.class));
    }

    @Override
    public Cryptography getCryptography() {
        return jdbcTemplate.queryForObject(SqlQueries.GET_CRYPTOGRAPHY,new MapSqlParameterSource(),BeanPropertyRowMapper.newInstance(Cryptography.class));
    }


}
