package dao.impl;

import dao.BankDao;
import dao.SqlQueries;
import domain.BankAccounts;
import domain.ResponseBankAccounts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class BankDaoImpl implements BankDao {


    private NamedParameterJdbcTemplate jdbcTemplate;
    @Autowired
    @Qualifier("dataSource")

    @Override
    public void setJdbc(DataSource jdbc) {
        this.jdbcTemplate=new NamedParameterJdbcTemplate(jdbc);
    }

    @Override
    public boolean addBankAccounts(BankAccounts bankAccounts) {
        return jdbcTemplate.update(SqlQueries.ADD_BANK,new BeanPropertySqlParameterSource(bankAccounts))==1;
    }

    @Override
    public boolean checkBankAccount(BankAccounts bankAccounts) {
        return jdbcTemplate.queryForObject(SqlQueries.GET_BANK_ACCOUNT,new BeanPropertySqlParameterSource(bankAccounts),Integer.class)>0;
    }

    @Override
    public boolean editBankAccounts(BankAccounts bankAccounts) {
        return jdbcTemplate.update(SqlQueries.EDIT_BANK,new BeanPropertySqlParameterSource(bankAccounts))==1;
    }

    @Override
    public boolean deleteBankAccounts(BankAccounts bankAccounts) {
        return jdbcTemplate.update(SqlQueries.DELETE_BANK,new BeanPropertySqlParameterSource(bankAccounts))==1;
    }

    @Override
    public List<ResponseBankAccounts> getBankList(String transactionId) {
        return  jdbcTemplate.query(SqlQueries.GET_BANK_LIST,
                new MapSqlParameterSource("transactionId", transactionId),
                BeanPropertyRowMapper.newInstance(ResponseBankAccounts.class)) ;
    }


}
