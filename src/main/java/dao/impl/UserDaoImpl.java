package dao.impl;

import dao.SqlQueries;
import dao.UserDao;
import domain.User;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import javax.sql.DataSource;

@Repository
public class UserDaoImpl implements UserDao {



    private NamedParameterJdbcTemplate jdbcTemplate;
    @Autowired
    @Qualifier("dataSource")
    @Override
    public void setJdbc(DataSource jdbc) {
        this.jdbcTemplate=new NamedParameterJdbcTemplate(jdbc);
    }

    @Override
    public boolean addUser(User user) {
        return jdbcTemplate.update(SqlQueries.INSERT_USER,new BeanPropertySqlParameterSource(user))==1;
    }

    @Override
    public boolean checkUser(User user) {

        return jdbcTemplate.queryForObject(SqlQueries.GET_USER_COUNT, new BeanPropertySqlParameterSource(user), Integer.class) > 0;

    }

    @Override
    public boolean isSession(User user) {
        return jdbcTemplate.queryForObject(SqlQueries.GET_USER_SESSION,new BeanPropertySqlParameterSource(user),Integer.class)>0;
    }

    @Override
    public boolean saveUser(User user) {
        return jdbcTemplate.update(SqlQueries.UPDATE_USER,new BeanPropertySqlParameterSource(user))==1;
    }

    @Override
    public boolean getUser(User user) {
        return jdbcTemplate.queryForObject(SqlQueries.CHECK_USER_COUNT, new BeanPropertySqlParameterSource(user), Integer.class) > 0;
    }

    @Override
    public boolean applyUser(User user) {
        return jdbcTemplate.update(SqlQueries.SAVE_USER,new BeanPropertySqlParameterSource(user))==1;
    }




    @Override
    public boolean checkUserData(User user) {
        return jdbcTemplate.queryForObject(SqlQueries.CHECK_USER, new BeanPropertySqlParameterSource(user), Integer.class) > 0;
    }



    @Override
    public Integer getCountDown() {
        return jdbcTemplate.queryForObject(SqlQueries.GET_COUNTDOWN, new MapSqlParameterSource(), Integer.class);
    }

    @Override
    public Map<String, Object> getUserByTrId(String trId, String msisdn,Integer roleId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("trId", trId);
        params.addValue("msisdn", msisdn);
        params.addValue("roleId",roleId);
        if(roleId==1){
            jdbcTemplate.update(SqlQueries.UPDATE_TRANSACTION_OWNER, params);
        }
        else{
            jdbcTemplate.update(SqlQueries.UPDATE_TRANSACTION_USER, params);
        }



        return jdbcTemplate.queryForMap(SqlQueries.SELECT_USER, params);
    }

    @Override
    public boolean checkRole(int roleId) {
        MapSqlParameterSource param=new MapSqlParameterSource();
        param.addValue("roleId",roleId);
        return jdbcTemplate.queryForObject(SqlQueries.CHECK_ROLE,param,Integer.class)>0;
    }

    @Override
    public User getPhone(String transactionId) {
        MapSqlParameterSource param=new MapSqlParameterSource();
        param.addValue("trId",transactionId);
        return  jdbcTemplate.queryForObject(SqlQueries.GET_PHONE_BY_TRANSACTION,param,BeanPropertyRowMapper.newInstance(User.class));
    }

    @Override
    public boolean addSubscribe(String email) {
        MapSqlParameterSource param=new MapSqlParameterSource();
        param.addValue("mail",email);
        return jdbcTemplate.update(SqlQueries.ADD_SUBSCRIBE,param)>0;
    }

}
