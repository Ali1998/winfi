package dao.impl;

import dao.ReportDao;
import dao.SqlQueries;
import domain.Operation;
import domain.Report;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import javax.sql.DataSource;


@Repository
public class ReportDaoImpl implements ReportDao {

    private NamedParameterJdbcTemplate jdbcTemplate;
    @Autowired
    @Qualifier("dataSource")

    @Override
    public void setJdbc(DataSource jdbc) {
        this.jdbcTemplate=new NamedParameterJdbcTemplate(jdbc);
    }

    @Override
    public Report getTodayReport(String transactionId) {
        return jdbcTemplate.queryForObject(SqlQueries.TODAY_REPORT,new MapSqlParameterSource("transactionId",transactionId), BeanPropertyRowMapper.newInstance(Report.class));
    }

    @Override
    public Report getMonthlyReport(String transactionId) {
        return jdbcTemplate.queryForObject(SqlQueries.MONTHLY_REPORT,new MapSqlParameterSource("transactionId",transactionId), BeanPropertyRowMapper.newInstance(Report.class));
    }

    @Override
    public Report getYearlyReport(String transactionId) {
        return jdbcTemplate.queryForObject(SqlQueries.YEARLY_REPORT,new MapSqlParameterSource("transactionId",transactionId),BeanPropertyRowMapper.newInstance(Report.class));
    }

    @Override
    public Report getTotalReport(String transactionId) {
        return jdbcTemplate.queryForObject(SqlQueries.TOTAL_REPORT,new MapSqlParameterSource("transactionId",transactionId), BeanPropertyRowMapper.newInstance(Report.class));
    }

    @Override
    public Report getTotalEarning(String transactionId) {
        return jdbcTemplate.queryForObject(SqlQueries.TOTAL_EARNING,new MapSqlParameterSource("transactionId",transactionId), BeanPropertyRowMapper.newInstance(Report.class));
    }

    @Override
    public Report getTodayEarning(String transactionId) {
        return jdbcTemplate.queryForObject(SqlQueries.TODAY_EARNING,new MapSqlParameterSource("transactionId",transactionId), BeanPropertyRowMapper.newInstance(Report.class));
    }

    @Override
    public Operation getPendingInfo(String transactionId) {

        MapSqlParameterSource param=new MapSqlParameterSource();
        param.addValue("transactionId",transactionId);
        return jdbcTemplate.queryForObject(SqlQueries.PENDING_INFORMATION,param,BeanPropertyRowMapper.newInstance(Operation.class));
    }

    @Override
    public Operation getSuccessInfo(String transactionId) {
        MapSqlParameterSource param=new MapSqlParameterSource();
        param.addValue("transactionId",transactionId);
        return jdbcTemplate.queryForObject(SqlQueries.SUCCESS_INFORMATION,param,BeanPropertyRowMapper.newInstance(Operation.class));

    }

    @Override
    public Operation getConnectionMaxDate(String transactionId) {

        MapSqlParameterSource param=new MapSqlParameterSource();
        param.addValue("transactionId",transactionId);
        return jdbcTemplate.queryForObject(SqlQueries.WIFI_CONNECT_MAX_DATE,param,BeanPropertyRowMapper.newInstance(Operation.class));
    }


}
