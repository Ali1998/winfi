package dao.impl;

import dao.SqlQueries;
import dao.WifiDao;
import domain.ResponseWifi;
import domain.Wifies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class WifiDaoImpl implements WifiDao {

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    @Qualifier("dataSource")
    @Override
    public void SetJdbC(DataSource jdbc) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(jdbc);
    }

    @Override
    public List<ResponseWifi> getWifiList(String transactionId) {
        return  jdbcTemplate.query(SqlQueries.GET_WIFI_LIST,
                new MapSqlParameterSource("transactionId", transactionId),
                BeanPropertyRowMapper.newInstance(ResponseWifi.class));

    }

    @Override
    public boolean addWifi(Wifies wifies) {
        return jdbcTemplate.update(SqlQueries.ADD_WIFI,new BeanPropertySqlParameterSource(wifies))==1;
    }

    @Override
    public boolean checkWifi(Wifies wifies) {
        return jdbcTemplate.queryForObject(SqlQueries.GET_WIFI,new BeanPropertySqlParameterSource(wifies),Integer.class)>0;
    }

    @Override
    public boolean editWifi(Wifies wifies) {
        return jdbcTemplate.update(SqlQueries.EDIT_WIFI,new BeanPropertySqlParameterSource(wifies))==1;
    }

    @Override
    public boolean deleteWifi(Wifies wifies) {
        return jdbcTemplate.update(SqlQueries.DELETE_WIFI,new BeanPropertySqlParameterSource(wifies))==1;
    }

    @Override
    public boolean checkWifi(String name,String password,Integer id) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("name",name);
        params.addValue("password",password);
        params.addValue("id",id);
        return jdbcTemplate.queryForObject(SqlQueries.CHECK_WIFI,params,Integer.class)>0;
    }

    @Override
    public boolean addConnect(Wifies wifies, String transactionId, double price) {
        MapSqlParameterSource param=new MapSqlParameterSource();
        param.addValue("wifiId", wifies.getId());
        param.addValue("transactionId",transactionId);
        param.addValue("price",price);
        return jdbcTemplate.update(SqlQueries.WIFI_CONNECT,param)>0;
    }

    @Override
    public boolean currentWifi(Wifies wifi) {
        return jdbcTemplate.queryForObject(SqlQueries.CHECK_CURRENT_WIFI,new BeanPropertySqlParameterSource(wifi),Integer.class)>0;
    }

    @Override
    public boolean currentCountryCode(String countryCode) {
        MapSqlParameterSource param = new MapSqlParameterSource();
        param.addValue("countryCode",countryCode);
        return jdbcTemplate.queryForObject(SqlQueries.CHECK_COUNTRY_CODE,param,Integer.class)>0 ;
    }




}


