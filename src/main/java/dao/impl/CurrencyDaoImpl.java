package dao.impl;

import dao.CurrencyDao;
import dao.SqlQueries;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Repository
public class CurrencyDaoImpl implements CurrencyDao {

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    @Qualifier("dataSource")
    @Override
    public void setJdbc(DataSource dataSource) {
        this.jdbcTemplate=new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public boolean addCurrency(Map<String, Object> map) {
        int count=0;
        Map<String,Object>[]currency = new Map[map.size()];

        try{
            for(Map.Entry<String,Object>entry:map.entrySet()){

                Map<String,Object>objectMap=new HashMap<>();
                objectMap.put("name",entry.getKey());
                objectMap.put("value",entry.getValue());
                currency[count]=objectMap;
                count++;
            }
            jdbcTemplate.batchUpdate(SqlQueries.ADD_CURRENCY,currency);
            return true;

        }catch (Exception ex){
            return false;
        }
    }

    @Override
    public boolean updateCurrency(Map<String, Object>currencyMap) {
        int count=1;
        Map<String,Object>[]currency = new Map[currencyMap.size()];

        try{
            for(Map.Entry<String,Object>entryMap:currencyMap.entrySet()){

                Map<String,Object>map=new HashMap<>();
                map.put("currency",entryMap.getKey());
                map.put("value",entryMap.getValue());
                map.put("id",count);
                currency[count-1]=map;
                count++;
            }
            jdbcTemplate.batchUpdate(SqlQueries.EDIT_CURRENCY,currency);
            return true;

        }catch (Exception ex){
            return false;
        }
    }

}
