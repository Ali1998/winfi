package dao.impl;

import dao.CardDao;
import dao.SqlQueries;
import domain.BankAccounts;
import domain.CardAccount;
import domain.ResponseCardAccounts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;


@Repository
public class CardDaoImpl implements CardDao {

    private NamedParameterJdbcTemplate jdbcTemplate;
    @Autowired
    @Qualifier("dataSource")

    @Override
    public void setJdbc(DataSource jdbc) {
        this.jdbcTemplate=new NamedParameterJdbcTemplate(jdbc);

    }

    @Override
    public List<ResponseCardAccounts> cardAccounts(String transactionId) {
        return  jdbcTemplate.query(SqlQueries.GET_CARD_LIST,
                new MapSqlParameterSource("transactionId", transactionId),
                BeanPropertyRowMapper.newInstance(ResponseCardAccounts.class));
    }



    @Override
    public boolean addCartAccounts(CardAccount cardAccount) {
        return jdbcTemplate.update(SqlQueries.ADD_CART,new BeanPropertySqlParameterSource(cardAccount))==1;
    }

    @Override
    public boolean checkCartAccount(CardAccount cardAccount) {
        return jdbcTemplate.queryForObject(SqlQueries.GET_CART_ACCOUNT,new BeanPropertySqlParameterSource(cardAccount),Integer.class)>0;
    }

    @Override
    public boolean editCartAccounts(CardAccount cardAccount) {
        return jdbcTemplate.update(SqlQueries.EDIT_CART,new BeanPropertySqlParameterSource(cardAccount))==1;
    }

    @Override
    public boolean deleteCardAccounts(CardAccount cardAccount) {
        return jdbcTemplate.update(SqlQueries.DELETE_CART,new BeanPropertySqlParameterSource(cardAccount))==1;
    }
}
