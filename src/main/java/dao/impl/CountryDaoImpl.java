package dao.impl;

import dao.CountryDao;
import dao.SqlQueries;
import domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.SQLWarningException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class CountryDaoImpl implements CountryDao {


    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    @Qualifier("dataSource")
    @Override
    public void setJdbc(DataSource jdbc) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(jdbc);
    }

    @Override
    public List<CountryWifi> getCountryWifiList(long id) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id", id);
        return jdbcTemplate.query(SqlQueries.GET_COUNTRY_WIFI_LIST, params, BeanPropertyRowMapper.newInstance(CountryWifi.class));
    }

    @Override
    public boolean checkCountry(String transactionId, Country country) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("transactionId", transactionId);
        params.addValue("id", country.getId());
        return jdbcTemplate.queryForObject(SqlQueries.CHECK_COUNTRY, params, Integer.class) > 0;
    }

    @Override
    public List<OwnedCountries> getOwnedCountry(String transactionId) {

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("transactionId", transactionId);
        List<OwnedCountries> ocList = jdbcTemplate.query(SqlQueries.OWNED_COUNTRIES, params, BeanPropertyRowMapper.newInstance(OwnedCountries.class));

        for (OwnedCountries countries : ocList) {
            List<OwnedWifi> wifis;
            MapSqlParameterSource param = new MapSqlParameterSource();
            param.addValue("cId", countries.getId());
            wifis = jdbcTemplate.query(SqlQueries.GET_USER_CONNECT_WIFI, param, BeanPropertyRowMapper.newInstance(OwnedWifi.class));
            countries.setWifi(wifis);

        }


        return ocList;
    }

    @Override
    public boolean currentCountry(Country country) {
        return jdbcTemplate.queryForObject(SqlQueries.CHECK_CURRENT_COUNTRY,new BeanPropertySqlParameterSource(country),Integer.class)>0;
    }

    @Override
    public boolean checkCountryWifi(String transactionId, long id) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("transactionId", transactionId);
        params.addValue("id", id);
        String sql=SqlQueries.CHECK_WIFI_COUNTRY+" and cr.finish_date>now()";
        return jdbcTemplate.queryForObject(sql, params, Integer.class) > 0;
    }

    @Override
    public boolean checkCountryWifiConnect(String transactionId, long id) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("transactionId", transactionId);
        params.addValue("id", id);
        return jdbcTemplate.queryForObject(SqlQueries.CHECK_WIFI_COUNTRY, params, Integer.class) > 0;
    }

    @Override
    public boolean localCheck(String phoneCode, int countryId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id",countryId);
        params.addValue("pCode", phoneCode);
        return jdbcTemplate.queryForObject(SqlQueries.LOCAL_OR_GLOBAL,params,Integer.class)>0;
    }


    @Override
    public List<Country> getCountryList() {
        List<Country>countries=jdbcTemplate.query(SqlQueries.GET_COUNTRY_LIST, BeanPropertyRowMapper.newInstance(Country.class));
        for(Country c:countries)
        {
            List<CountryWifi> wifi;
            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("id", c.getId());
            wifi = jdbcTemplate.query(SqlQueries.COUNTRY_WIFI, params, BeanPropertyRowMapper.newInstance(CountryWifi.class));
            c.setWifi(wifi);
        }
        return countries ;
    }



}
