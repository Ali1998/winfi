package dao;

public interface SqlQueries {



    ///User

    String INSERT_USER = "insert into users(smscode, msisdn,role_id,created_date) " +
            "values(:smscode, :msisdn,:roleId, now())";


    String GET_USER_COUNT="select count(id) from users where (transaction_owner_id=:transactionId or transaction_user_id=:transactionId) and status=1";

    String CHECK_USER="select count(id) from users where msisdn=:msisdn and status=1";

    String CHECK_USER_COUNT="select count(id) from users where msisdn=:msisdn and smscode=:smscode and role_id=:roleId and status=1";

    String GET_USER_SESSION="SELECT TIME_TO_SEC(timediff(now(), created_date))<(select count_down from settings) from users where msisdn=:msisdn and smscode=:smscode and status=1";

    String UPDATE_USER="update users set smscode=:smscode,role_id=:roleId,created_date=now() where msisdn=:msisdn and status=1";

    String SAVE_USER="update users set username=:username where (transaction_owner_id=:transactionId or transaction_user_id=:transactionId) and status=1";

    String SELECT_USER = "select username," +
            "(case when role_id=1 then transaction_owner_id else transaction_user_id end ) transactionId " +
            "from users where (transaction_owner_id=:trId or transaction_user_id=:trId) and role_id=:roleId and status=1";


    String UPDATE_TRANSACTION_OWNER="update users set transaction_owner_id=:trId where msisdn=:msisdn and status=1";

    String UPDATE_TRANSACTION_USER="update users set transaction_user_id=:trId where msisdn=:msisdn and status=1";

    String CHECK_ROLE="select count(id) from role where roleId=:roleId";





    /////Wifies





    String GET_WIFI_LIST="select w.id,name,location,description,password,address,mac_address macAddress from winfi.wifies w, " +
            "winfi.users s where s.id=w.createdby_id and s.transaction_owner_id=:transactionId and w.status=1";


    String ADD_WIFI="insert into wifies(name,location,password,address,description,mac_address,country_code,created_date,createdby_id)" +
            "values(:name,:location,:password,:address,:description,:macAddress,:countryCode,now(),(" +
            "select id from users where transaction_owner_id=:transactionId))";


    String GET_WIFI="select count(w.id) from wifies w,winfi.users s where s.id=w.createdby_id and " +
            "w.id=:id and s.transaction_owner_id=:transactionId";


    String EDIT_WIFI="update wifies set name=:name,location=:location,address=:address," +
            " description=:description,password=:password,created_date=now() where id=:id";


    String DELETE_WIFI="update wifies set status=0 where id=:id";

    String CHECK_WIFI="select count(id) from wifies where name=:name and password=:password " +
            "and id!=:id and status=1";

    String CHECK_CURRENT_WIFI="select count(w.id) from wifies w where w.id=:id and w.status=1";








    ///bank


    String GET_BANK_LIST="select b.id,name,iban from winfi.bankaccounts b,winfi.users s where s.id=b.user_id and s.transaction_owner_id=:transactionId and b.status=1";


    String ADD_BANK="insert into bankaccounts(name,iban,create_date,user_id) values(:name,:iban,now(),(select id from users where transaction_owner_id=:transactionId))";


    String EDIT_BANK="update bankaccounts set name=:name,iban=:iban,create_date=now() where id=:id";

    String GET_BANK_ACCOUNT="select count(b.id) from bankaccounts b,winfi.users s where s.id=b.user_id and b.id=:id and s.transaction_owner_id=:transactionId and b.status=1";

    String DELETE_BANK="update bankaccounts set status=0 where id=:id";



    ///card;


    String GET_CARD_LIST="select c.id,name,cardnumber from cardaccounts c,winfi.users s where s.id=c.created_byId and s.transaction_owner_id=:transactionId and c.status=1";


    String ADD_CART="insert into cardaccounts(name,cardnumber,created_date,created_byId) values(:name,:cardnumber,now(),(select id from users where transaction_owner_id=:transactionId))";


    String EDIT_CART="update cardaccounts set name=:name,cardnumber=:cardnumber,created_date=now() where id=:id";

    String GET_CART_ACCOUNT="select count(c.id) from cardaccounts c,winfi.users s where s.id=c.created_byId and c.id=:id and s.transaction_owner_id=:transactionId and c.status=1";

    String DELETE_CART="update cardaccounts set status=0 where id=:id";





    //Settings

    String GET_COUNTDOWN="select count_down from settings limit 1";



    //Report

    String TODAY_REPORT="select  count(r.id) todayCount,date_format(date(now()),\"%Y.%m.%d\") todayDate from winfi.reports r,wifies w\n" +
            "where w.id=r.wifi_id  and  w.createdby_id=(select id from users where transaction_owner_id=:transactionId)  and date(r.connect_date)=date(now())";

    String MONTHLY_REPORT="select  count(r.id) monthlyCount,monthname(now()) monthlyDate from winfi.reports r,wifies w " +
            "  where w.id=r.wifi_id  and w.createdby_id=(select id from users where transaction_owner_id=:transactionId) " +
            "  and month(r.connect_date)=month(now())";

    String YEARLY_REPORT="select  count(*) yearlyCount,year(now()) yearlyDate from winfi.reports r,wifies w " +
            "  where w.id=r.wifi_id  and  w.createdby_id=(select id from users where transaction_owner_id=:transactionId)" +
            "  and year(r.connect_date)=year(now())";


    String TOTAL_REPORT=" select  count(*) totalCount,(select concat(date_format( min(date(connect_date)),\"%Y.%m.%d\"),\"-\",date_format(max(date(connect_date)),\"%Y.%m.%d\") )) totalDate  from winfi.reports r,wifies w\n" +
            "where w.id=r.wifi_id  and  w.createdby_id=(select id from users where transaction_owner_id=:transactionId) ";


    String TODAY_EARNING="select " +
            "(case  when sum(r.price) is null then 0 else sum(r.price) end) todayEarning, " +
            "date_format(date(now()),\"%Y.%m.%d\") todayDate from reports r,users u,wifies w " +
            "where w.createdby_id=u.id and w.id=r.wifi_id and u.transaction_owner_id=:transactionId " +
            "and date(r.connect_date)=date(now())";


    String TOTAL_EARNING="select (case when sum(r.price) is null then 0 else sum(r.price) end) totalEarning," +
            "(select concat(date_format( min(date(connect_date)),\"%Y.%m.%d\"),\"-\",date_format(max(date(connect_date)),\"%Y.%m.%d\") )) totalDate " +
            "from reports r,users u,wifies w " +
            "where w.createdby_id=u.id and w.id=r.wifi_id and " +
            "u.transaction_owner_id=:transactionId ";


    String PENDING_INFORMATION="select " +
            "date_format(max(date(p.create_date)),\"%Y.%m.%d\") pendingDate," +
            "(case when sum(p.amount) is null then 0 else sum(p.amount) end) amount " +
            "from payment p " +
            "left outer join cardaccounts ca on p.card_id=ca.id " +
            "left outer join bankaccounts ba on p.bank_id=ba.id " +
            "left outer join users u on case when p.card_id is null then u.id=ba.user_id " +
            "else u.id=ca.created_byId end " +
            "where u.transaction_owner_id=:transactionId and p.status=0";



    String SUCCESS_INFORMATION="select " +
            "date_format(max(date(p.result_date)),\"%Y.%m.%d\") resultDate, " +
            "(case when sum(p.amount) is null then 0 else sum(p.amount) end) amount " +
            "from payment p " +
            "left outer join cardaccounts ca on p.card_id=ca.id " +
            "left outer join bankaccounts ba on p.bank_id=ba.id " +
            "left outer join users u on case when p.card_id is null then u.id=ba.user_id  " +
            "else u.id=ca.created_byId end  " +
            "where u.transaction_owner_id=:transactionId and p.status=1";


    String WIFI_CONNECT_MAX_DATE="select date_format(max(r.connect_date),'%Y.%m.%d') maxDate from reports r,users u,wifies w " +
            "where w.createdby_id=u.id and w.id=r.wifi_id and u.transaction_owner_id=:transactionId";






    //Country

    String GET_COUNTRY_LIST="select distinct c.id, c.country_name name from country c,wifies w " +
            "where  c.status=1 " +
            "and c.country_code=w.country_code and w.status=1";

    String GET_COUNTRY_WIFI_LIST="select w.location from winfi.wifies w,winfi.country c " +
            "where w.country_code=c.country_code and c.id=:id and w.status=1";

    String CHECK_COUNTRY="select count(cr.id)  " +
            "from country_report cr, users u  " +
            "where cr.country_id=:id and cr.user_id=u.id  " +
            "and cr.finish_date>now() and u.transaction_user_id=:transactionId";


    String OWNED_COUNTRIES="select UNIX_TIMESTAMP(cr.finish_date) finishDate ,time_to_sec(timediff(cr.finish_date,now()))finishTime ,c.id,c.country_name from winfi.country_report cr,users u,country c \n" +
            " where cr.country_id=c.id and cr.user_id=u.id " +
            " and u.transaction_user_id=:transactionId and cr.finish_date>now()";

    String GET_USER_CONNECT_WIFI="select w.name,w.mac_address macAddress,w.id,w.address,w.location,w.description,w.password from winfi.wifies w,country c " +
            "where c.country_code=w.country_code and (c.id=:cId || c.id=242) and w.status=1";


    String COUNTRY_WIFI="select w.location,w.name,w.address from winfi.wifies w,country c " +
            "   where c.country_code=w.country_code and c.id=:id and w.status=1";


    String WIFI_CONNECT="insert into winfi.reports(wifi_id,user_id,price)" +
            "values(:wifiId,(select id from winfi.users where transaction_user_id=:transactionId),:price)";



    String CHECK_CURRENT_COUNTRY="SELECT count(c.id)  FROM winfi.country c,winfi.wifies w " +
            "where c.id=:id and w.country_code=c.country_code and w.status=1";

    String CHECK_COUNTRY_CODE="SELECT count(id)  FROM winfi.country " +
            " where country_code=:countryCode";


    String CHECK_WIFI_COUNTRY="select count(cr.id) " +
            " from country_report cr, users u,wifies w,country c " +
            " where cr.country_id=c.id and c.country_code=w.country_code and w.id=:id and cr.user_id=u.id  " +
            " and u.transaction_user_id=:transactionId and w.status=1";





    //Payment


     String ADD_PAYMENT="insert into winfi.payment(card_id,bank_id,amount)" +
             " values(:cardId,:bankId,:amount)";


     String CHECK_CARD_INFORMATION="select count(c.id) from cardaccounts c,winfi.users s " +
             "where s.id=c.created_byId and c.id=:cardId and s.transaction_owner_id=:transactionId and c.status=1";

     String CHECK_BANK_INFORMATION="select count(b.id) from bankaccounts b, winfi.users s " +
             "where s.id=b.user_id and b.id=:bankId and s.transaction_owner_id=:transactionId and b.status=1";

     String GET_PACKAGES="select p.day_count duration,p.price,p.id packId,round((cr.convertAzn* p.price),2) convertPrice," +
             "cr.name currencyName from packages p,country c,currency cr " +
             "where p.country_id=:countryId and c.id=p.country_id and p.status=1 and type=:type " +
             "and cr.id=p.currency_id " +
             "order by p.day_count " +
             " ";

     String GET_PACKAGES_BY_ID="select p.day_count duration,p.country_id countryId,p.price,p.id packId from packages p " +
             "where p.id=:packId and p.status=1";

     String JOIN_COUNTRY="insert into country_report(country_id,user_id,finish_date,price,pack_id) " +
             "values(:countryId,(select u.id from users u where u.transaction_user_id=:transactionId),now()+interval :duration day,:price,:packId)";


     String CHECK_CONNECT_COUNTRY="select count(cr.id) from country_report cr,users u " +
             "where cr.user_id=u.id and u.transaction_user_id=:transactionId and " +
             "cr.finish_date>now() and cr.country_id=:countryId";




     //Configuration

    String SELECT_CONFIG="SELECT cost,amount from winfi.configuration limit 1";



    //Transactions

    String GET_TRANSACTIONS_LIST=" select case when p.card_id is null then ba.name else ca.name end accountName, " +
            "case when p.card_id is null then 'Bank' else 'Card' end typename," +
            "p.description,date(p.create_date) createdDate,p.status,p.amount " +
            "from payment p " +
            "left outer join cardaccounts ca on p.card_id=ca.id  " +
            "left outer join bankaccounts ba on p.bank_id=ba.id " +
            "left outer join users u on case when p.card_id is null then u.id=ba.user_id " +
            "else u.id=ca.created_byId end " +
            "where u.transaction_owner_id=:transactionId";




    //Cryptography



    String GET_CRYPTOGRAPHY="select c.key,c.init_vector initVector from cryptography c " +
            "limit 1";





    //local and global packages

    String GET_PHONE_BY_TRANSACTION="select id,msisdn from users " +
            "where transaction_user_id=:trId";

    String LOCAL_OR_GLOBAL="select count(id) from country " +
            "where id=:id and phone_code=:pCode";

    //subscribe

    String ADD_SUBSCRIBE="insert into subscribe(mail) " +
            "values(:mail)";



    //valyuta

    String ADD_CURRENCY="insert into currency(name,convertAzn,created_date)" +
            "values(:name,:value,now())";

    String EDIT_CURRENCY="update currency set name=:currency,convertAzn=:value,created_date=now() where id=:id";












}
