package dao;

import domain.BankAccounts;
import domain.ResponseBankAccounts;

import javax.sql.DataSource;
import java.util.List;

public interface BankDao {

     List<ResponseBankAccounts>getBankList(String transactionId);

    void setJdbc(DataSource jdbc);


    boolean addBankAccounts(BankAccounts bankAccounts);

    boolean checkBankAccount(BankAccounts bankAccounts);

    boolean editBankAccounts(BankAccounts bankAccounts);

    boolean deleteBankAccounts(BankAccounts bankAccounts);



}
