package dao;

import domain.Country;
import domain.CountryWifi;
import domain.OwnedCountries;

import javax.sql.DataSource;
import java.util.List;
import java.util.Map;

public interface CountryDao {


    List<Country> getCountryList();
    void setJdbc(DataSource jdbc);

    List<CountryWifi> getCountryWifiList(long id);

    boolean checkCountry(String transactionId,Country country);


    List<OwnedCountries> getOwnedCountry(String transactionId);


    boolean currentCountry(Country country);

    boolean checkCountryWifi(String transactionId,long id);

    boolean checkCountryWifiConnect(String transactionId,long id);

    boolean localCheck(String phoneCode,int countryId);








}
