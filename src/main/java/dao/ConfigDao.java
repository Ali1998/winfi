package dao;

import domain.Configuration;
import domain.Cryptography;

import javax.sql.DataSource;
import java.util.Map;

public interface ConfigDao {


    void setJdbc(DataSource jdbc);

    Configuration getConfig();

    Cryptography getCryptography();




}
