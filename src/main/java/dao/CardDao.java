package dao;

import domain.BankAccounts;
import domain.CardAccount;
import domain.ResponseCardAccounts;

import javax.sql.DataSource;
import java.util.List;

public interface CardDao {

    List<ResponseCardAccounts> cardAccounts(String transactionId);

    void setJdbc(DataSource jdbc);


    boolean addCartAccounts(CardAccount cardAccount);

    boolean checkCartAccount(CardAccount cardAccount);

    boolean editCartAccounts(CardAccount cardAccount);

    boolean deleteCardAccounts(CardAccount cardAccount);

}
