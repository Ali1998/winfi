package dao;

import domain.User;

import javax.sql.DataSource;
import java.util.Map;

public interface UserDao {

    void setJdbc(DataSource  jdbc);

    boolean  addUser(User user);

    boolean checkUser(User user);

    boolean isSession(User user);

    boolean saveUser(User user);

    boolean getUser(User user);

    boolean applyUser(User user);

    boolean checkUserData(User user);

    Integer getCountDown();

    Map<String, Object> getUserByTrId(String trId, String msisdn,Integer roleId);

    boolean checkRole(int roleId);

    User getPhone(String transactionId);

    boolean addSubscribe(String email);






}
