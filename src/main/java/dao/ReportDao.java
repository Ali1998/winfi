package dao;

import domain.Operation;
import domain.Report;

import javax.sql.DataSource;

public interface ReportDao {

     void setJdbc(DataSource jdbc);

     Report getTodayReport(String transactionId);

     Report getMonthlyReport(String transactionId);

     Report getYearlyReport(String transactionId);

     Report getTotalReport(String transactionId);

     Report getTotalEarning(String transactionId);

     Report getTodayEarning(String transactionId);

     Operation getPendingInfo(String transactionId);

     Operation getSuccessInfo(String transactionId);

     Operation getConnectionMaxDate(String transactionId);







}
