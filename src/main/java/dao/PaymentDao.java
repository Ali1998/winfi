package dao;

import java.util.List;

import domain.Packages;
import domain.PackagesBuy;
import domain.Transactions;

import javax.sql.DataSource;

public interface PaymentDao {

    void setJdbc(DataSource jdbc);

    boolean addPayment(Integer bankId,Integer cardId,Double amount);

    boolean checkPaymentAccount(Integer bankId,Integer cardId,String transactionId);

    List<Transactions> getTransactions(String fromDate,String toDate,Integer status,String transactionId) throws Exception;

    List<Packages> getPackages(long id,int type);

    PackagesBuy getPackageById(long id);

    boolean connectCountry(PackagesBuy p,String transactionId);

    boolean checkConnectCountry(String transactionId,Integer countryId);








}
