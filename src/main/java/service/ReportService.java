package service;


import dao.ReportDao;
import domain.Operation;
import domain.Report;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReportService {

    @Autowired
    private ReportDao reportDao;


   public Report getTodayReport(String transactionId){return reportDao.getTodayReport(transactionId);}

   public Report getMonthlyReport(String transactionId){return reportDao.getMonthlyReport(transactionId);}

   public Report getYearlyReport(String transactionId){return reportDao.getYearlyReport(transactionId);}

   public Report getTotalReport(String transactionId){return reportDao.getTotalReport(transactionId);}

   public Operation getPendingInfo(String transactionId){return reportDao.getPendingInfo(transactionId);}

   public Operation getSuccessInfo(String transactionId){return reportDao.getSuccessInfo(transactionId);}

   public Operation getConnectionMaxDate(String transactionId){return reportDao.getConnectionMaxDate(transactionId);}

   public Report getTotalEarning(String transactionId){return reportDao.getTotalEarning(transactionId);}

   public Report getTodayEarning(String transactionId){return reportDao.getTodayEarning(transactionId);}

}
