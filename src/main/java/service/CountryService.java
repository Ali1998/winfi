package service;


import dao.CountryDao;
import domain.Country;
import domain.CountryWifi;
import domain.OwnedCountries;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class CountryService {
    @Autowired
    private CountryDao countryDao;


    public List<Country> getCountryList(){return countryDao.getCountryList();}

    public List<CountryWifi> getCountryWifiList(long id){return countryDao.getCountryWifiList(id);}

    public  boolean checkCountry(String transactionId,Country country){return countryDao.checkCountry(transactionId,country);}

    public List<OwnedCountries> getOwnedCountry(String transactionId){return countryDao.getOwnedCountry(transactionId);}

    public boolean currentCountry(Country country){return countryDao.currentCountry(country);}

    public boolean checkCountryWifi(String transactionId,long id){return countryDao.checkCountryWifi(transactionId,id);}

    public boolean checkCountryWifiConnect(String transactionId, long id){return countryDao.checkCountryWifiConnect(transactionId,id);}

    public boolean localCheck(String phoneCode, int countryId){return countryDao.localCheck(phoneCode,countryId);}



}
