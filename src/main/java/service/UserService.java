package service;

import dao.SqlQueries;
import dao.UserDao;
import domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class UserService {
    @Autowired
    private UserDao userDao;

    public boolean addUser(User user){ return userDao.addUser(user);}

    public boolean  checkUser(User user){return userDao.checkUser(user);}


    public boolean isSession(User user){
        return userDao.isSession(user);
    }

    public boolean saveUser(User user) { return userDao.saveUser(user);}

    public boolean getUser(User user) { return userDao.getUser(user);}

    public boolean applyUser(User user) { return userDao.applyUser(user);}

    public  Integer getCountDown(){return userDao.getCountDown();}

    public  boolean checkUserData(User user){return userDao.checkUserData(user);}

    public Map<String, Object> getUserByTrId(String trId, String msisdn,Integer roleId){ return userDao.getUserByTrId(trId, msisdn,roleId); }

    public boolean checkRole(int roleId){return userDao.checkRole(roleId);}

    public User getPhone(String transactionId){return userDao.getPhone(transactionId);}

    public boolean addSubscribe(String email){return userDao.addSubscribe(email);}
}
