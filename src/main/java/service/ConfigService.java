package service;

import dao.ConfigDao;
import domain.Configuration;
import domain.Cryptography;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class ConfigService {

    @Autowired
    private ConfigDao configDao;

    public Configuration getConfig(){return configDao.getConfig();}

    public Cryptography getCryptography() {return configDao.getCryptography();}
}
