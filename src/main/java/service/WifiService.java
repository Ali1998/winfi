package service;

import dao.WifiDao;
import domain.ResponseWifi;
import domain.Wifies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class WifiService {

    @Autowired
    private WifiDao wifiDao;

    public List<ResponseWifi>getWifiList(String transactionId){
        return wifiDao.getWifiList(transactionId);
    }

    public boolean addWifi(Wifies wifies){return wifiDao.addWifi(wifies);}

    public boolean checkWifi(Wifies wifies) {return wifiDao.checkWifi(wifies);}

    public boolean editWifi(Wifies wifies){return wifiDao.editWifi(wifies);}

    public boolean deleteWifi(Wifies wifies){return wifiDao.deleteWifi(wifies);}

    public boolean checkWifi(String name,String password,Integer id) {return wifiDao.checkWifi(name,password,id);}

    public   boolean addConnect(Wifies wifies, String transactionId, double price){return wifiDao.addConnect(wifies,transactionId,price);}

    public  boolean currentWifi(Wifies wifi){return wifiDao.currentWifi(wifi);}

    public boolean currentCountryCode(String countryCode){return wifiDao.currentCountryCode(countryCode);}





}
