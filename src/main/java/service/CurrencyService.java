package service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import dao.CurrencyDao;
import domain.Config;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class CurrencyService {

    @Autowired
    private CurrencyDao currencyDao;



    public HttpClientService clientService=new HttpClientService();

    public boolean currency(int type){

        Map<String,Object>objectMap =new HashMap<>();
        ObjectMapper mapper=new ObjectMapper();

        try {

            String response=clientService.sendGet(Config.CURRENCY_URL);
            JSONObject json=new JSONObject(response).getJSONObject("quotes");
            Map<String,Object> map=mapper.readValue(String.valueOf(json), new TypeReference<Map<String, String>>() {
            });


            Double azn=Double.parseDouble(map.get("USDAZN").toString());
            for(Map.Entry<String,Object>entry:map.entrySet()){

                String name=entry.getKey().substring(3,entry.getKey().length());
                Double value=Double.parseDouble(entry.getValue().toString());
                value=(azn)/100/value;
                objectMap.put(name,value);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

         if(type==1){

            return currencyDao.addCurrency(objectMap);

         }else if(type==2){

             return currencyDao.updateCurrency(objectMap);
         }else{
             return false;
         }

    }



}
