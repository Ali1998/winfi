package service;

import dao.CardDao;
import domain.CardAccount;
import domain.ResponseCardAccounts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CardService {
    @Autowired
    private CardDao cardDao;

    public List<ResponseCardAccounts> cardAccounts(String transactionId){return cardDao.cardAccounts(transactionId);}

    public boolean addCartAccounts(CardAccount cardAccount){return cardDao.addCartAccounts(cardAccount);};

    public boolean checkCartAccount(CardAccount cardAccount){return cardDao.checkCartAccount(cardAccount);};

    public boolean editCartAccounts(CardAccount cardAccount){return cardDao.editCartAccounts(cardAccount);};

    public boolean deleteCardAccounts(CardAccount cardAccount){return cardDao.deleteCardAccounts(cardAccount);};
}
