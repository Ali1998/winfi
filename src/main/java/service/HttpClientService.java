package service;
import domain.RequestDomain;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.InputStreamReader;

@Service
public class HttpClientService {
    private static final Logger logger = LogManager.getLogger(HttpClientService.class);
    // HTTP POST request
    public String sendPost(String url, RequestDomain data) throws Exception {
        try {
            RequestConfig defaultRequestConfig = RequestConfig.custom()
                    .setSocketTimeout(10000)
                    .setConnectTimeout(10000)
                    .setConnectionRequestTimeout(10000)
                    .build();
            CloseableHttpResponse httpResponse = null;
            CloseableHttpClient httpClient = HttpClients.custom().setDefaultRequestConfig(defaultRequestConfig).build();
            HttpPost httpPost = new HttpPost(url);
            httpPost.setHeader("Content-Type", "application/json;charset=UTF-8");
            StringEntity requestEntity = new StringEntity(new JSONObject(data).toString(),
                    ContentType.APPLICATION_JSON);

            httpPost.setEntity(requestEntity);
            try {
                httpResponse = httpClient.execute(httpPost);
            } catch (Exception e) {
                logger.error("Error while sending Post Request. ", e);
            }
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    httpResponse.getEntity().getContent(), "UTF-8"));
            String inputLine;
            String line = "";
            while ((inputLine = reader.readLine()) != null) {
                line += inputLine;
            }
            logger.info("HTTP Client result line : " + line);
            reader.close();
            httpClient.close();
            return line;
        } catch (Exception e) {
            logger.error("Error occured : ", e);
            return null;
        }

    }
    // HTTP GET request
    public String sendGet(String url) throws Exception {
        try {
            RequestConfig defaultRequestConfig = RequestConfig.custom()
                    .setSocketTimeout(5000)
                    .setConnectTimeout(5000)
                    .setConnectionRequestTimeout(5000)
                    .build();
            CloseableHttpResponse httpResponse = null;
            CloseableHttpClient httpClient = HttpClients.custom().setDefaultRequestConfig(defaultRequestConfig).build();
            HttpGet httpGet = new HttpGet(url);
            try {
                httpResponse = httpClient.execute(httpGet);
            } catch (Exception e) {
                logger.error("Error while sending Get Request. ", e);
            }
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    httpResponse.getEntity().getContent()));
            String inputLine;
            String fullText = "";
            while ((inputLine = reader.readLine()) != null) {
                fullText += inputLine;
            }
            reader.close();
            httpClient.close();
            return fullText;
        } catch (Exception e) {
            logger.error("Error occured : ", e);
            e.printStackTrace();
            return  null;
        }
    }
}