package service;

import dao.BankDao;
import domain.BankAccounts;
import domain.ResponseBankAccounts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BankService {

   @Autowired
    private BankDao bankDao;

    public List<ResponseBankAccounts> getBankList(String transactionId){return bankDao.getBankList(transactionId);}

    public boolean addBankAccounts(BankAccounts bankAccounts){return bankDao.addBankAccounts(bankAccounts);}

    public boolean checkBankAccount(BankAccounts bankAccounts){return bankDao.checkBankAccount(bankAccounts);}

    public boolean editBankAccounts(BankAccounts bankAccounts){ return bankDao.editBankAccounts(bankAccounts);}

    public boolean deleteBankAccounts(BankAccounts bankAccounts){ return  bankDao.deleteBankAccounts(bankAccounts);}
}
