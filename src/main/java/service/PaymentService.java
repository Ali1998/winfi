package service;

import dao.PaymentDao;
import domain.Packages;
import domain.PackagesBuy;
import domain.Payment;
import domain.Transactions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaymentService {

    @Autowired
    private PaymentDao paymentDao;

    public boolean addPayment(Integer bankId,Integer cardId,Double amount){return paymentDao.addPayment(bankId,cardId,amount);}

    public boolean checkPaymentAccount(Integer bankId,Integer cardId,String transactionId){return paymentDao.checkPaymentAccount(bankId,cardId,transactionId);}

    public List<Transactions> getTransactions(String fromDate, String toDate, Integer status,String transactionId) throws Exception{return paymentDao.getTransactions(fromDate,toDate,status,transactionId);}

    public List<Packages>getPackages(long id,int type){return paymentDao.getPackages(id,type);}

    public PackagesBuy getPackageById(long id){return paymentDao.getPackageById(id);}

    public boolean connectCountry(PackagesBuy p,String transactionId){return paymentDao.connectCountry(p,transactionId);}

    public boolean checkConnectCountry(String transactionId, Integer countryId){return paymentDao.checkConnectCountry(transactionId,countryId);}





}
